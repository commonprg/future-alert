<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TemplateController;
use App\Http\Controllers\CourceController;
use App\Http\Controllers\PaymentController;
use Stichoza\GoogleTranslate\GoogleTranslate;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\NursingRecruitmentController;
use App\Http\Controllers\Controller;
use App\Http\Controllers\RazorpayController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     $tr = new GoogleTranslate(); // Translates to 'en' from auto-detected language by default
//     $tr->setSource('fr');
//     return $tr->translate('Hello World!');
//     return view('welcome');
// });

Route::get('/', [TemplateController::class, 'Home'])->name('/');
// Route::get('login', [TemplateController::class, 'login'])->name('login');
Route::get('checkout', [TemplateController::class, 'checkouts'])->name('checkout');
Route::post('checkout', [TemplateController::class, 'addUser'])->name('checkout');
Route::get('resetpassword', [TemplateController::class, 'resetpassword'])->name('resetpassword');
Route::get('take-course', [CourceController::class, 'checkouts'])->name('take-course');
Route::get('contact-us', [TemplateController::class, 'contactUs'])->name('contact-us');
Route::get('privacy-policy', [TemplateController::class, 'privacyPolicy'])->name('privacy-policy');
Route::get('pricing', [TemplateController::class, 'pricing'])->name('pricing');
Route::get('about-us', [TemplateController::class, 'aboutUs'])->name('about-us');
Route::get('terms-condition', [TemplateController::class, 'termsCondition'])->name('terms-condition');
Route::get('psc-coaching', [TemplateController::class, 'pscCoaching'])->name('psc-coaching');
Route::get('ket-coaching', [TemplateController::class, 'ketCoaching'])->name('ket-coaching');

Route::post('payment', [PaymentController::class, 'index'])->name('payment');
Route::post('payment-verify', [PaymentController::class, 'paymentVerify'])->name('payment-verify');
Route::post('payment-response', [PaymentController::class, 'paymentResponse'])->name('payment-response');
Route::get('refund-order', [PaymentController::class, 'refundOrder'])->name('refund-order');

Route::get('lang/{lang}', ['as' => 'lang.switch', 'uses' => 'App\Http\Controllers\LanguageController@switchLang']);

// Route::middleware(['auth:sanctum', 'verified', 'isUser'])->get('/userdashboard', function () {
//     return view('dashboard');
// })->name('dashboard');

Route::middleware(['auth:sanctum', 'verified', 'isUser'])->group(function () {
  Route::get('userdashboard', [CourceController::class, 'index'])->name('dashboard');
  Route::get('docdownload/{filename}', [CourceController::class, 'docdownload'])->name('docdownload'); 
  Route::get('docview/{filename}', [CourceController::class, 'docview'])->name('docview'); 
  // Route::get('documentview', [AdminController::class, 'documentview'])->name('admin.documentview');
});

Route::middleware(['auth', 'isAdmin'])->group(function () {
    Route::get('admin', [AdminController::class, 'index'])->name('admin');
    Route::get('dashboard', [AdminController::class, 'index'])->name('admin.dashboard');
    Route::get('users', [AdminController::class, 'getUsers'])->name('admin.getUsers');
    Route::get('Meeting', [AdminController::class, 'Meeting'])->name('admin.Meeting');
    Route::post('crtMeeting', [AdminController::class, 'crtMeeting'])->name('admin.crtMeeting');
    Route::get('dateRange', [AdminController::class, 'dateRange'])->name('admin.dateRange');
    Route::get('ParticipantsView/{id}', [AdminController::class, 'ParticipantsView'])->name('admin.ParticipantsView');
    Route::get('getMeetings', [AdminController::class, 'getMeetings'])->name('admin.getMeetings');  
    Route::get('viewParticipants/{id}', [AdminController::class, 'viewParticipants'])->name('admin.viewParticipants');
    Route::get('editMeeting/{id}', [AdminController::class, 'editMeeting'])->name('admin.editMeeting');
    Route::post('meetingupdate/{id}', [AdminController::class, 'meetingupdate'])->name('admin.meetingupdate');
    Route::get('getCandidates', [AdminController::class, 'getCandidates'])->name('admin.getCandidates');    
    Route::get('download/{filename}', [AdminController::class, 'download'])->name('admin.download'); 
    Route::get('getHospitals', [AdminController::class, 'getHospitals'])->name('admin.getHospitals');  
    Route::get('getHospitallist', [AdminController::class, 'getHospitallist'])->name('admin.getHospitallist'); 
    Route::get('addHospital', [AdminController::class, 'addHospital'])->name('admin.addHospital'); 
    Route::post('addHos', [AdminController::class, 'addHos'])->name('admin.addHos');   
    Route::get('view/{id}', [AdminController::class, 'view'])->name('admin.view');
    Route::get('edit/{id}', [AdminController::class, 'edit'])->name('admin.edit');
    Route::get('delete/{id}', [AdminController::class, 'delete'])->name('admin.delete');
    Route::post('update/{id}', [AdminController::class, 'update'])->name('admin.update');
    Route::get('getEduloanUsers', [AdminController::class, 'getEduloanUsers'])->name('admin.getEduloanUsers');    
    Route::get('profileView/{id}', [AdminController::class, 'profileView'])->name('admin.profileView');
    Route::post('selectedHospital', [AdminController::class, 'selectedHospital'])->name('admin.selectedHospital');
    Route::get('getApplications', [AdminController::class, 'getApplications'])->name('admin.getApplications');
    Route::post('selectedList', [AdminController::class, 'selectedList'])->name('admin.selectedList');
    Route::get('getShortlistedCandidate', [AdminController::class, 'getShortlistedCandidate'])->name('admin.getShortlistedCandidate');
    Route::post('replyEmail', [AdminController::class, 'replyEmail'])->name('admin.replyEmail');
    Route::post('candidateDocUpload', [AdminController::class, 'candidateDocUpload'])->name('admin.candidateDocUpload');
  });

Route::get('recruitment', [NursingRecruitmentController::class, 'index'])->name('recruitment');
Route::post('recruit', [NursingRecruitmentController::class, 'store'])->name('recruit');

Route::post('razorpaypayment', [RazorpayController::class, 'razorpaypayment'])->name('razorpaypayment');