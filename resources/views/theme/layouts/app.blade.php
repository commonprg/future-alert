@include('theme.layouts.header')

<!-- BEGIN: Content Dynamically -->
@yield('content')
<!-- END: Content -->

@include('theme.layouts.footer')

<script src="//cdn.bootcss.com/jquery/2.2.4/jquery.min.js"></script>
<script src="//cdn.bootcss.com/toastr.js/latest/js/toastr.min.js"></script>
@if($message = Session::get('success'))
    <script type="text/javascript">
        toastr.success("{{ $message }}", "Success", {
            progressBar: !0
        })
    </script>
@endif
</body>
</html>