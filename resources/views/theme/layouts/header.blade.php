<!DOCTYPE html>
    <html lang="en">

    <head>
        <meta charset="utf-8" />
        <title>Future @ Alert - Academy</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- favicon -->
        <link rel="shortcut icon" href="{{ asset('frontend/assets/images/favicon.ico') }}"> 
        <!-- Bootstrap -->
        <link href="{{ asset('frontend/assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
        <!-- Icons -->
        <link href="{{ asset('frontend/assets/css/materialdesignicons.min.css') }}" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="https://unicons.iconscout.com/release/v2.1.9/css/unicons.css">
        <!-- Slider -->               
        <link rel="stylesheet" href="{{ asset('frontend/assets/css/owl.carousel.min.css') }}"/> 
        <link rel="stylesheet" href="{{ asset('frontend/assets/css/owl.theme.default.min.css') }}"/> 
        <!-- Main Css -->
        <link href="{{ asset('frontend/assets/css/style.css') }}" rel="stylesheet" type="text/css" id="theme-opt" />
        <link href="{{ asset('frontend/assets/css/colors/default.css') }}" rel="stylesheet" id="color-opt">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <!-- Toastr -->
        <link rel="stylesheet" type="text/css" 
        href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">  
        <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    </head>
    <style>
        .modal-icon{
            position: absolute;
            top: 1%;
            right: 1%;
            color: #000;
        }
        .vimeo-checkout-section{
                    padding-top: 200px!important;
                    text-align: -webkit-center;
                    margin-bottom: 100px;
                }
            @media only screen and (max-width: 400px) {
                .vimeo-checkout-section{
                    padding-top: 100px!important;
                }
            }
            @media only screen and (min-width: 401px) and (max-width: 460px){
                .vimeo-checkout-section{
                    padding-top: 120px!important;
                } 
             }  
            @media only screen and (min-width: 1317px){
                .vimeo-checkout-section{
                    padding-top: 300px!important;
                }
            }
    </style>
    <script>
       $(window).on( "load", function() {
            sessionModal = Object.keys(sessionStorage);
            if (sessionModal == "") {
                jQuery.noConflict();
                $('#exampleModalCenter').modal({show:true, backdrop: 'static', keyboard: false});
            }
            $('#exampleModalCenter').on('shown.bs.modal', function () {
                sessionStorage.setItem('modal', 'true');
            })
            
        }); 
    </script>
    <body>
        <!-- Loader -->
        <!-- <div id="preloader">
            <div id="status">
                <div class="spinner">
                    <div class="double-bounce1"></div>
                    <div class="double-bounce2"></div>
                </div>
            </div>
        </div> -->
        <!-- Loader -->
        @if (!\Request::is('recruitment'))
        <div id="carbonads">
            <span>
                <span class="carbon-wrap">
                    <a href="{{ route('recruitment') }}" class="carbon-img" rel="noopener sponsored">
                        <img src="{{ asset('frontend/assets/images/nurse-job.png') }}" alt="" height="100" width="130" style="max-width: 130px;">
                    </a>
                    <a href="{{ route('recruitment') }}" class="carbon-text" target="_blank" rel="noopener sponsored">Your new nursing career awaits. Click to register.</a>
                </span>
            </span>
        </div>
        @endif

        <style>
            #carbonads {
            position: fixed;
            bottom: 20px;
            right: 20px;
            background-color: #F7F8FB;
            z-index: 100;
            box-shadow: 0 0 1px hsla(0, 0%, 0%, 0.25);
            padding: .625em;
            max-width: 130px;
            box-sizing: content-box;
            border-radius:5px;
            }

            #carbonads span {
                display: block;
            }

            #carbonads img {
                display: block;
                margin-bottom: .75em;
            }

            #carbonads .carbon-text {
                display: block;
                font-size: .75em;
                line-height: 1.4em;
                color: #2b2e38;
            }

            #carbonads .carbon-poweredby {
                font-size: .5em;
                text-transform: uppercase;
                letter-spacing: 1px;
                line-height: 1;
                color: #777777;
            }

            /* @media only screen and (min-width: 320px) and (max-width: 1027px) {
                #carbonads {
                    position: relative;
                    float: none;
                    bottom: initial;
                    right: initial;
                    margin: 20px 0;
                    max-width: 330px;
                    padding: 1em;
                }
                #carbonads .carbon-wrap {
                display: flex;
                flex-direction: row;
                }
                #carbonads img {
                margin: 0 1em 0 0;
                }
                #carbonads .carbon-text {
                font-size: .825em;
                margin-bottom: 1em;
                }
                #carbonads .carbon-poweredby {
                position: absolute;
                left: 162px;
                bottom: 15px;
                }
            } */

            @media only screen and (min-width: 320px) and (max-width: 429px) {
                #carbonads .carbon-wrap {
                flex-direction: column;
                }
                #carbonads img {
                margin: initial;
                margin: 0 0 .5em 0;
                }
                #carbonads .carbon-text {
                margin-bottom: initial;
                }
                #carbonads .carbon-poweredby {
                position: initial;
                left: initial;
                right: initial;
                }
            }

            @media only print {
                #carbonads {
                    display: none;
                }
            }
        </style>
        
        <!-- Navbar STart -->
        <header id="topnav" class="defaultscroll sticky">
            <div class="container-fluid">
                <!-- Logo container-->
                <div>
                    <a class="logo pl-4 pt-4" href="{{ route('/') }}">
                        <img src="{{ asset('frontend/assets/images/falert.png') }}" class="l-dark" height="24" alt="">
                        <img src="{{ asset('frontend/assets/images/falert.png') }}" class="l-light" height="24" alt="">
                    </a>
                </div>            
                <div class="buy-button pt-4">
                    <a href="{{ route('login') }}">
                        @if (Auth::user())
                            <div class="btn btn-primary login-btn-primary">Dashboard</div>
                            <div class="btn btn-light login-btn-light">Dashboard</div>
                        @else
                            <div class="btn btn-primary login-btn-primary">Login</div>
                            <div class="btn btn-light login-btn-light">Login</div>
                        @endif        
                    </a>
                </div><!--end login button-->
                <!-- End Logo container-->
                <div class="menu-extras">
                    <div class="menu-item">
                        <!-- Mobile menu toggle-->
                        <a class="navbar-toggle">
                            <div class="lines">
                                <span></span>
                                <span></span>
                                <span></span>
                            </div>
                        </a>
                        <!-- End mobile menu toggle-->
                    </div>
                </div>
        
                <div id="navigation">
                    <!-- Navigation Menu-->   
                    <ul class="navigation-menu {{ Request::is('/') ? 'nav-light' : '' }}">
                        <li><a href="{{ route('/') }}">Home</a></li>
                        
                        <li><a href="{{ route('about-us') }}">About Us</a></li>
                        <li class="has-submenu">
                            <a href="javascript:void(0)">Courses</a><span class="menu-arrow"></span>
                            <ul class="submenu megamenu">
                                <li>
                                    <ul>
                                        <li><a href="{{ route('psc-coaching') }}">PSC Coaching</a></li>
                                        <li><a href="{{ route('ket-coaching') }}">K-TET Coaching</a></li>
                                    </ul>
                                </li>  
                            </ul>
                        </li>
                        <li><a href="{{ route('contact-us') }}">Contact Us</a></li>
                        <li><a href="{{ route('pricing') }}">Pricing</a></li>
                        <li class="has-submenu">
                            <a href="javascript:void(0)">Company</a><span class="menu-arrow"></span>
                            <ul class="submenu megamenu">
                                <li>
                                    <ul>
                                        <li><a href="{{ route('privacy-policy') }}">Privicy Policy</a></li>
                                        <li><a href="{{ route('terms-condition') }}">Terms and Condition</a></li>
                                    </ul>
                                </li>  
                            </ul>
                        </li>
                        @if(count(config('app.languages')) > 1)
                        <li class="has-submenu"><a href="javascript:void(0)"> {{ config('language')[App::getLocale()] }}</a><span class="submenu-arrow"></span>
                            <ul class="submenu">
                            
                            @foreach (config('language') as $lang => $language)
                                @if ($lang != App::getLocale())
                                        <a class="dropdown-item" href="{{ route('lang.switch', $lang) }}"> {{$language}}</a>
                                @endif
                            @endforeach

                            </ul>  
                        </li>
                        @endif
                    </ul> <!--end navigation menu-->
                    
                    <!--end login button-->
                </div><!--end navigation-->
            </div><!--end container-->
        </header><!--end header-->
        <!-- Navbar End -->
        