@extends('theme.layouts.app')
@section('headerClass','')
@section('content')
<section class="bg-half bg-light d-table w-100">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-12 text-center">
                        <div class="page-next-level">
                            <img src="{{ asset('frontend/assets/images/job/Gradle.svg') }}" class="avatar avatar-small" alt="">
                            <h4 class="title mt-4 mb-3"> K-TET COACHING </h4>
                            <p class="para-desc mx-auto text-muted">We areconducting the best-rated K-TET coaching Center in Kerala, for our valuable students who are the main objective of our coaching center. Our qualified faculty is providing the various classes as per the course to be completed within time and students must be able to achieve goals in the examination.</p>
                            
                        </div>
                    </div>  <!--end col-->
                </div><!--end row-->
            </div> <!--end container-->
        </section><!--end section-->
        <!-- Hero End -->

        <!-- Shape Start -->
        <div class="position-relative">
            <div class="shape overflow-hidden text-white">
                <svg viewBox="0 0 2880 48" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M0 48H1437.5H2880V0H2160C1442.5 52 720 0 720 0H0V48Z" fill="currentColor"></path>
                </svg>
            </div>
        </div>
        <!--Shape End-->
        
        <!-- Job Detail Start -->
        <section class="section">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4 col-md-5 col-12">
                        <div class="card sidebar sticky-bar rounded shadow border-0">
                            <div class="card-body widget border-bottom">
                                <h5 class="mb-0"> Information</h5>
                            </div>
    
                            <div class="card-body">
                                <div class="media widget align-items-center">
                                    <i data-feather="user-check" class="fea icon-ex-md mr-3"></i>
                                    <div class="media-body">
                                        <h4 class="widget-title mb-0">Coaching Time:</h4>
                                        <p class="text-primary mb-0">Full Time & Part Time</p>
                                    </div>
                                </div>

                                <div class="media widget align-items-center mt-3">
                                    <i data-feather="map-pin" class="fea icon-ex-md mr-3"></i>
                                    <div class="media-body">
                                        <h4 class="widget-title mb-0">Location:</h4>
                                        <p class="text-primary mb-0">Currently in Online</p>
                                    </div>
                                </div>


                                <div class="media widget align-items-center mt-3">
                                    <i data-feather="book" class="fea icon-ex-md mr-3"></i>
                                    <div class="media-body">
                                        <h4 class="widget-title mb-0">Qualifications:</h4>
                                        <p class="text-primary mb-0">Not Required</p>
                                    </div>
                                </div>

                                <div class="media widget align-items-center mt-3">
                                    <i data-feather="dollar-sign" class="fea icon-ex-md mr-3"></i>
                                    <div class="media-body">
                                        <h4 class="widget-title mb-0">Cost:</h4>
                                        <p class="text-primary mb-0">7999</p>
                                    </div>
                                </div>


                            </div>
                        </div>
                    </div><!--end col-->
    
                    <div class="col-lg-8 col-md-7 col-12 mt-4 mt-sm-0 pt-2 pt-sm-0">
                        <div class="ml-lg-4">
                            <h5>Course Description: </h5>
                            <p class="text-muted">KTET is conducted by Kerala State Education Board (KSEB) to recruit candidates for teaching profiles in lower primary, upper primary and high school profile vacancies in Kerala. ... KTET Exam is conducted to recruit teachers for primary classes (1-5), Upper Primary classes (6-8) and Secondary classes (9-10)..</p>
                            <p class="text-muted">
Posts Offered: As per the official website of KTET, separate exams will be conducted for selection of teachers for different categories of Category-I (Lower Primary Teachers), Category-II (Upper Primary Teachers), Category-III (High School Teachers) and Category-IV (language Teaching like Arabic, Hindi, Sanskrit, Urdu etc, Specialist Teachers (Art & Craft), Physical Education Teachers).</p>
                            <p class="text-muted">Best in class study materials. We assist your pursuit with well researched study materials prepared by the expert hands in the Kerala K-TET arena.</p>
                            
                            <h5 class="mt-4">Class Highlights: </h5>
                            <ul class="list-unstyled">
                                <li class="text-muted"><i data-feather="arrow-right" class="fea icon-sm text-success mr-2"></i>Classes handled by Retd Officers and the Expert Teaching Faculties</li>
                                <li class="text-muted"><i data-feather="arrow-right" class="fea icon-sm text-success mr-2"></i>Face to Face Engagement and Individual Mentor Support</li>
                                <li class="text-muted"><i data-feather="arrow-right" class="fea icon-sm text-success mr-2"></i>Latest updated Course Content and Materials</li>
                                <li class="text-muted"><i data-feather="arrow-right" class="fea icon-sm text-success mr-2"></i>Get Book Materials & Handouts prepared by Research Team</li>
                                <li class="text-muted"><i data-feather="arrow-right" class="fea icon-sm text-success mr-2"></i>100% Result focussed Coaching & Training Policy</li>
                                <li class="text-muted"><i data-feather="arrow-right" class="fea icon-sm text-success mr-2"></i>Result oriented coaching and practice strategy will get you sure success</li>
                            </ul>

                            <div class="mt-4">
                                <a href="{{ route('take-course') }}" class="btn btn-outline-primary">Apply Now <i class="mdi mdi-send"></i></a>
                            </div>
                        </div>
                    </div><!--end col-->
                </div><!--end row-->
            </div><!--end container-->
        </section><!--end section-->
        <!-- Job Detail End -->
@endsection