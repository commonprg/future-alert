@extends('theme.layouts.app')
@section('headerClass','')
@section('content')
        <!-- Modal Start -->
        <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-body" style="padding:0">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <i class="material-icons modal-icon">close</i>
                        </button>
                        <a href="{{ route('checkout') }}"><img src="{{ asset('frontend/assets/images/Student.jpg') }}" alt="student_edu-loan" style="width: -webkit-fill-available;"></a>
                    </div>
                </div>
            </div>
        </div>
        <!-- Modal end -->
        <!-- Hero Start -->
        <section class="bg-half-260 bg-primary d-table w-100" style="background: url('frontend/assets/images/enterprise.png') center center;">
            <div class="bg-overlay"></div>
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="title-heading text-center mt-4">
                            <h1 class="display-4 title-dark font-weight-bold text-white mb-3">Future @ Alert</h1>
                            <p class="para-desc mx-auto text-white-50">Introducing a new initiate which is very helpful for higher education seekers for submitting their edu-loan & CSR Scholarship application through online</p>
                            <div class="mt-4 pt-2">
                                <a href="{{ route('take-course') }}" class="btn btn-primary"><i class="mdi mdi-email"></i> Get Started</a>
                            </div>
                        </div>
                    </div><!--end col-->
                </div><!--end row-->
            </div><!--end container--> 
        </section><!--end section-->
        <!-- Hero End -->

        <section class="bg-half-170 border-bottom d-table w-100" id="home">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-6 col-md-7">
                        <div class="title-heading mt-4">
                            <div class="alert alert-light alert-pills shadow" role="alert">
                                <span class="badge badge-pill badge-danger mr-1">v2.5</span>
                                <span class="content"> Build <span class="text-primary">anything</span> you want - Landrick.</span>
                            </div>
                            <h1 class="heading mb-3">Leading Digital Business For <span class="element text-primary" data-elements="Agency, Software, Technology, Studio, Webapps"></span> Solution</h1>
                            <p class="para-desc text-muted">Launch your campaign and benefit from our expertise on designing and managing conversion centered bootstrap4 html page.</p>
                            <div class="mt-4">
                                <a href="javascript:void(0)" class="btn btn-outline-primary rounded"><i class="mdi mdi-google-my-business"></i> Make Your Shop</a>
                            </div>
                        </div>
                    </div><!--end col-->

                    <div class="col-lg-6 col-md-5 mt-4 pt-2 mt-sm-0 pt-sm-0">
                        <div class="position-relative">
                            <img src="images/busi01.jpg" class="rounded img-fluid mx-auto d-block" alt="">
                            <div class="play-icon">
                            <div style="padding:75% 0 0 0;position:relative;"><iframe src="https://player.vimeo.com/video/649049744??autoplay=1&loop=1&autopause=0" frameborder="0" allow="autoplay; fullscreen" allowfullscreen style="position:absolute;top:0;left:0;width:100%;height:100%;"></iframe></div><script src="https://player.vimeo.com/api/player.js"></script>
                            </div>
                        </div>
                    </div><!--end col-->
                </div><!--end row-->
            </div><!--end container--> 
        </section><!--end section-->

        <!-- Shape Start -->
        <div class="position-relative">
            <div class="shape overflow-hidden text-white">
                <svg viewBox="0 0 2880 48" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M0 48H1437.5H2880V0H2160C1442.5 52 720 0 720 0H0V48Z" fill="currentColor"></path>
                </svg>
            </div>
        </div>
        <!--Shape End-->

        <!-- Feature Start -->
        <section class="section">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-12 text-center">
                        <div class="section-title mb-4 pb-2">
                            <h4 class="title mb-4">What We Do ?</h4>
                            <p class="text-muted para-desc mx-auto mb-0">We help and support of all languages in india to increase visibility ,efficiency,drive more scholarship and improve proffessional educational success. Guiding students a solution to their financial needs.</p>
                            <p class="text-muted para-desc mx-auto mb-0">We also offer classes for competitive exams such as PSC & KET Exam etc. Our team of excellent and experienced tutors, Charted Accounts, Advocates strive to provide quality online education with the help of brilliant class notes and different teaching styles. We guide our students at every step of their journey and help them to clear all the queries and doubts. We are devoted and committed to the excellence of education and knowledge. Future @ Alert is the best online coaching centre for this kind</p>
                        </div>
                    </div><!--end col-->
                </div><!--end row-->

                <div class="row">
                    <div class="col-md-4 col-12 mt-5">
                        <div class="features text-center">
                            <div class="image position-relative d-inline-block">
                                <img src="{{ asset('frontend/assets/images/icon/pen.svg') }}" class="avatar avatar-small" alt="">
                            </div>

                            <div class="content mt-4">
                                <h4 class="title-2">Matching Scholarship</h4>
                                <p class="text-muted mb-0">Intelligent Scholarship Matching & recommendation system</p>
                            </div>
                        </div>
                    </div><!--end col-->
                    
                    <div class="col-md-4 col-12 mt-5">
                        <div class="features text-center">
                            <div class="image position-relative d-inline-block">
                                <img src="{{ asset('frontend/assets/images/icon/user.svg') }}" class="avatar avatar-small" alt="">
                            </div>

                            <div class="content mt-4">
                                <h4 class="title-2">Multi- Lingual Support</h4>
                                <p class="text-muted mb-0">Scholarship information & Tele -Support in various regional languages..</p>
                            </div>
                        </div>
                    </div><!--end col-->
                    
                    <div class="col-md-4 col-12 mt-5">
                        <div class="features text-center">
                            <div class="image position-relative d-inline-block">
                                <img src="{{ asset('frontend/assets/images/icon/intellectual.svg') }}" class="avatar avatar-small" alt="">
                            </div>

                            <div class="content mt-4">
                                <h4 class="title-2">SMS & Email Notification</h4>
                                <p class="text-muted mb-0">Regular alerts on new / upcoming scholarships and during application process.</p>
                            </div>
                        </div>
                    </div><!--end col-->

                    <div class="col-md-4 col-12 mt-5">
                        <div class="features text-center">
                            <div class="image position-relative d-inline-block">
                                <img src="{{ asset('frontend/assets/images/icon/video.svg') }}" class="avatar avatar-small" alt="">
                            </div>

                            <div class="content mt-4">
                                <h4 class="title-2">Phone & Email Notification</h4>
                                <p class="text-muted mb-0">Dedicated support to facilitate application/documentation formalities or enquires..</p>
                            </div>
                        </div>
                    </div><!--end col-->
                    
                    <div class="col-md-4 col-12 mt-5">
                        <div class="features text-center">
                            <div class="image position-relative d-inline-block">
                                <img src="{{ asset('frontend/assets/images/icon/calendar.svg') }}" class="avatar avatar-small" alt="">
                            </div>

                            <div class="content mt-4">
                                <h4 class="title-2">Online Coaching</h4>
                                <p class="text-muted mb-0">Dedicated and intensive online coaching forsubmitting edu-load applicationand scholarship application through online.</p>
                            </div>
                        </div>
                    </div><!--end col-->
                    
                    <div class="col-md-4 col-12 mt-5">
                        <div class="features text-center">
                            <div class="image position-relative d-inline-block">
                                <img src="{{ asset('frontend/assets/images/icon/sand-clock.svg') }}" class="avatar avatar-small" alt="">
                            </div>

                            <div class="content mt-4">
                                <h4 class="title-2">Safe Guarding student's Right</h4>
                                <p class="text-muted mb-0">Drafting various complaints & processing applications unde RTI Act 2005,which will enable every students to safeguard their Rights.</p>
                            </div>
                        </div>
                    </div><!--end col-->
                </div><!--end row-->
            </div><!--end container-->

            
        </section><!--end section-->
        <!-- Feature End -->

        <section class="section">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-12 text-center">
                        <div class="section-title mb-4 pb-2"> 
                            <h5 class="title mb-4"><span class="text-primary font-weight-bold">EDU LOAN IS THE RIGHT OF STUDENTS - IF YOU BELIVE THIS, WE WILL HELP YOU TO PROTECT YOUR RIGHT. ASSURE YOU PROPER GUIDANCE $ ASSIATANCE FOR PROCESSING EDU LOAN & CSR SCHOLARSHIP APPLICATION THROUGH ONLINE</span></h5>
                            <p class="text-muted para-desc mb-0 mx-auto">Benefit of model educational loan scheme through online application</p>
                        </div>
                    </div><!--end col-->
                </div><!--end row-->

                <div class="row">
                    <div class="col-lg-3 col-md-4 mt-4 pt-2">
                        <div class="card features fea-primary rounded p-4 bg-light text-center position-relative overflow-hidden border-0">
                            <span class="h1 icon2 text-primary">
                                <i class="uil uil-chart-line"></i>
                            </span>
                            <div class="card-body p-0 content">
                                <h5>Online Application</h5>
                                <p class="para text-muted mb-0">The Department of financial Service has made it mandatory for all banks to accept education loan applications only through online.</p>
                            </div>
                            <span class="big-icon text-center">
                                <i class="uil uil-chart-line"></i>
                            </span>
                        </div>
                    </div><!--end col-->
                    
                    <div class="col-lg-3 col-md-4 mt-4 pt-2">
                        <div class="card features fea-primary rounded p-4 bg-light text-center position-relative overflow-hidden border-0">
                            <span class="h1 icon2 text-primary">
                                <i class="uil uil-crosshairs"></i>
                            </span>
                            <div class="card-body p-0 content">
                                <h5>Bank Details</h5>
                                <p class="para text-muted mb-0">Submit eduloan applicationthrough online with three banks at one time.</p>
                            </div>
                            <span class="big-icon text-center">
                                <i class="uil uil-crosshairs"></i>
                            </span>
                        </div>
                    </div><!--end col-->
                    
                    <div class="col-lg-3 col-md-4 mt-4 pt-2">
                        <div class="card features fea-primary rounded p-4 bg-light text-center position-relative overflow-hidden border-0">
                            <span class="h1 icon2 text-primary">
                                <i class="uil uil-airplay"></i>
                            </span>
                            <div class="card-body p-0 content">
                                <h5>Monitoring Service</h5>
                                <p class="para text-muted mb-0">Your login is and password help you to monitorthe process of your edu loan application..</p>
                            </div>
                            <span class="big-icon text-center">
                                <i class="uil uil-airplay"></i>
                            </span>
                        </div>
                    </div><!--end col-->
                    
                    <div class="col-lg-3 col-md-4 mt-4 pt-2">
                        <div class="card features fea-primary rounded p-4 bg-light text-center position-relative overflow-hidden border-0">
                            <span class="h1 icon2 text-primary">
                                <i class="uil uil-rocket"></i>
                            </span>
                            <div class="card-body p-0 content">
                                <h5>Info Of edu-loan</h5>
                                <p class="para text-muted mb-0">No bank Managers can reject / refuse any edu loan application without the consent of his / her higher authorities..</p>
                            </div>
                            <span class="big-icon text-center">
                                <i class="uil uil-rocket"></i>
                            </span>
                        </div>
                    </div><!--end col-->
                    
                    <div class="col-lg-3 col-md-4 mt-4 pt-2">
                        <div class="card features fea-primary rounded p-4 bg-light text-center position-relative overflow-hidden border-0">
                            <span class="h1 icon2 text-primary">
                                <i class="uil uil-clock"></i>
                            </span>
                            <div class="card-body p-0 content">
                                <h5>Sanction Time</h5>
                                <p class="para text-muted mb-0">Your edu loan application will be sanctioned/ rejected with in 15 -30 days's time periods. If rejected the reason should be menstioned while rejecting it.</p>
                            </div>
                            <span class="big-icon text-center">
                                <i class="uil uil-clock"></i>
                            </span>
                        </div>
                    </div><!--end col-->
                    
                    <div class="col-lg-3 col-md-4 mt-4 pt-2">
                        <div class="card features fea-primary rounded p-4 bg-light text-center position-relative overflow-hidden border-0">
                            <span class="h1 icon2 text-primary">
                                <i class="uil uil-users-alt"></i>
                            </span>
                            <div class="card-body p-0 content">
                                <h5>Documents</h5>
                                <p class="para text-muted mb-0">Edu loans below Rs. 75 lakhs co- lateral security is not required.</p>
                            </div>
                            <span class="big-icon text-center">
                                <i class="uil uil-users-alt"></i>
                            </span>
                        </div>
                    </div><!--end col-->
                    
                    <div class="col-lg-3 col-md-4 mt-4 pt-2">
                        <div class="card features fea-primary rounded p-4 bg-light text-center position-relative overflow-hidden border-0">
                            <span class="h1 icon2 text-primary">
                                <i class="uil uil-file-alt"></i>
                            </span>
                            <div class="card-body p-0 content">
                                <h5>Cibil Score issue</h5>
                                <p class="para text-muted mb-0">Parent's CIBIL score or liability will not be considered.</p>
                            </div>
                            <span class="big-icon text-center">
                                <i class="uil uil-file-alt"></i>
                            </span>
                        </div>
                    </div><!--end col-->
                    
                    <div class="col-lg-3 col-md-4 mt-4 pt-2">
                        <div class="card features fea-primary rounded p-4 bg-light text-center position-relative overflow-hidden border-0">
                            <span class="h1 icon2 text-primary">
                                <i class="uil uil-search"></i>
                            </span>
                            <div class="card-body p-0 content">
                                <h5>Other</h5>
                                <p class="para text-muted mb-0">In case of injustice by the concerned bank managers, the applicant students or parents can approach the higher authorities of the bank for re dresser of the grievances> Nobody will stop you from this.</p>
                            </div>
                            <span class="big-icon text-center">
                                <i class="uil uil-search"></i>
                            </span>
                        </div>
                    </div><!--end col-->
                </div><!--end row-->
            </div><!--end container-->
        </section>

        <!-- Shape Start -->
        <div class="position-relative">
            <div class="shape overflow-hidden text-white">
                <svg viewBox="0 0 2880 48" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M0 48H1437.5H2880V0H2160C1442.5 52 720 0 720 0H0V48Z" fill="currentColor"></path>
                </svg>
            </div>
        </div>
        <!--Shape End-->

        <!-- Price start -->
        <section class="section">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-12 text-center">
                        <div class="section-title mb-4 pb-2">
                            <h4 class="title mb-4">EDU- LOAN ?.... CSR SCOLARSHIP.?</h4>
                            <p class="text-muted para-desc mb-0 mx-auto">Introducing a new initiate which is very helpful for higher education seekers for submitting their eduloan & C S R Scholarship application through online.</p>
                        </div>
                    </div><!--end col-->
                </div><!--end row-->

                <div class="row justify-content-center">
                    <!--<div class="col-lg-3 col-md-6 col-12 mt-4 pt-2">
                        <div class="card pricing-rates business-rate shadow bg-light border-0 rounded">
                            <div class="card-body">
                                <h2 class="title text-uppercase mb-4">Free</h2>
                                <div class="d-flex mb-4">
                                    <span class="h4 mb-0 mt-2">$</span>
                                    <span class="price h1 mb-0">0</span>
                                    <span class="h4 align-self-end mb-1">/mo</span>
                                </div>
                                
                                <ul class="list-unstyled mb-0 pl-0">
                                    <li class="h6 text-muted mb-0"><span class="text-primary h5 mr-2"><i class="uim uim-check-circle"></i></span>Full Access</li>
                                    <li class="h6 text-muted mb-0"><span class="text-primary h5 mr-2"><i class="uim uim-check-circle"></i></span>Source Files</li>
                                </ul>
                                <a href="javascript:void(0)" class="btn btn-primary mt-4">Buy Now</a>
                            </div>
                        </div>
                    </div>--><!--end col-->
                    
                    <!--<div class="col-lg-3 col-md-6 col-12 mt-4 pt-2">
                        <div class="card pricing-rates business-rate shadow bg-white border-0 rounded">
                            <div class="card-body">
                                <h2 class="title text-uppercase text-primary mb-4">Starter</h2>
                                <div class="d-flex mb-4">
                                    <span class="h4 mb-0 mt-2">$</span>
                                    <span class="price h1 mb-0">39</span>
                                    <span class="h4 align-self-end mb-1">/mo</span>
                                </div>

                                <ul class="list-unstyled mb-0 pl-0">
                                    <li class="h6 text-muted mb-0"><span class="text-primary h5 mr-2"><i class="uim uim-check-circle"></i></span>Full Access</li>
                                    <li class="h6 text-muted mb-0"><span class="text-primary h5 mr-2"><i class="uim uim-check-circle"></i></span>Source Files</li>
                                    <li class="h6 text-muted mb-0"><span class="text-primary h5 mr-2"><i class="uim uim-check-circle"></i></span>Free Appointments</li>
                                </ul>
                                <a href="javascript:void(0)" class="btn btn-primary mt-4">Get Started</a>
                            </div>
                        </div>
                    </div>--><!--end col-->
                    
                    <!-- <div class="col-lg-3 col-md-6 col-12 mt-4 pt-2">
                        <div class="card pricing-rates business-rate shadow bg-light border-0 rounded">
                            <div class="card-body">
                                <h2 class="title text-uppercase mb-4">Professional</h2>
                                <div class="d-flex mb-4">
                                    <span class="h4 mb-0 mt-2">$</span>
                                    <span class="price h1 mb-0">59</span>
                                    <span class="h4 align-self-end mb-1">/mo</span>
                                </div>
                                
                                <ul class="list-unstyled mb-0 pl-0">
                                    <li class="h6 text-muted mb-0"><span class="text-primary h5 mr-2"><i class="uim uim-check-circle"></i></span>Full Access</li>
                                    <li class="h6 text-muted mb-0"><span class="text-primary h5 mr-2"><i class="uim uim-check-circle"></i></span>Source Files</li>
                                    <li class="h6 text-muted mb-0"><span class="text-primary h5 mr-2"><i class="uim uim-check-circle"></i></span>1 Domain Free</li>
                                    <li class="h6 text-muted mb-0"><span class="text-primary h5 mr-2"><i class="uim uim-check-circle"></i></span>Enhanced Security</li>
                                </ul>
                                <a href="javascript:void(0)" class="btn btn-primary mt-4">Try It Now</a>
                            </div>
                        </div>
                    </div> --><!--end col-->
                    
                    <div class="col-lg-3 col-md-6 col-12 mt-4 pt-2">
                        <div class="card pricing-rates business-rate shadow bg-light border-0 rounded">
                            <div class="card-body">
                                <h2 class="title text-uppercase mb-4">Edu-loan Class</h2>
                                <div class="d-flex mb-0 text-center" style="height:45px">
                                    <span class="h4 mb-0 mt-2">&#8377</span>
                                    <del style="color:#373738;"><span class="price h1 mb-0" style="color:red;">2999</span></del>&nbsp;&nbsp;&nbsp;
                                    <span class="h6 mb-0 mt-2" style="color:green">&#8377</span>
                                    <span class="price h3 mb-0" style="color:green">999</span>
                                </div>   
                                <span class="h6 mt-2 offer-price" style="color:red">Save <span class="h6 mb-0 mt-2" style="color:red">&#8377</span>2000</span>
                                <ul class="list-unstyled mb-0 mt-3 pl-0">
                                    <li class="h6 text-muted mb-0"><span class="text-primary h5 mr-2"><i class="uim uim-check-circle"></i></span>Full Access</li>
                                    <li class="h6 text-muted mb-0"><span class="text-primary h5 mr-2"><i class="uim uim-check-circle"></i></span>Daily Classes</li>
                                    <li class="h6 text-muted mb-0"><span class="text-primary h5 mr-2"><i class="uim uim-check-circle"></i></span>Source File</li>
                                    <li class="h6 text-muted mb-0"><span class="text-primary h5 mr-2"><i class="uim uim-check-circle"></i></span>Zoom Classes</li>
                                    <li class="h6 text-muted mb-0"><span class="text-primary h5 mr-2"><i class="uim uim-check-circle"></i></span>Learing</li>
                                </ul>
                                <a href="{{ route('take-course') }}" class="btn btn-primary mt-4">Started Now</a>
                            </div>
                        </div>
                    </div><!--end col-->
                    <div class="col-lg-3 col-md-6 col-12 mt-4 pt-2" >
                        <div class="card pricing-rates business-rate shadow bg-light border-0 rounded">
                            <div class="card-body">
                                <h2 class="title text-uppercase mb-4">PSC Coaching</h2>
                                <div class="d-flex mb-4 text-center">
                                    <span class="h4 mb-0 mt-2">&#8377</span>
                                    <span class="price h1 mb-0 ">4999</span>
                                </div>
                                
                                <ul class="list-unstyled mb-0 pl-0">
                                    <li class="h6 text-muted mb-0"><span class="text-primary h5 mr-2"><i class="uim uim-check-circle"></i></span>Full Access</li>
                                    <li class="h6 text-muted mb-0"><span class="text-primary h5 mr-2"><i class="uim uim-check-circle"></i></span>Daily Classes</li>
                                    <li class="h6 text-muted mb-0"><span class="text-primary h5 mr-2"><i class="uim uim-check-circle"></i></span>Notes Proving</li>
                                    <li class="h6 text-muted mb-0"><span class="text-primary h5 mr-2"><i class="uim uim-check-circle"></i></span>Zoom Classes</li>
                                    <li class="h6 text-muted mb-0"><span class="text-primary h5 mr-2"><i class="uim uim-check-circle"></i></span>Learing</li>
                                </ul>
                                <a href="javascript:void(0)" class="btn btn-primary mt-4 disabled" style="pointer-events: none;background-color: #818181!important;
    border-color: #818181 !important;">Started Now</a>
                            </div>
                        </div>
                    </div><!--end col-->
                    <div class="col-lg-3 col-md-6 col-12 mt-4 pt-2">
                        <div class="card pricing-rates business-rate shadow bg-light border-0 rounded">
                            <div class="card-body">
                                <h2 class="title text-uppercase mb-4">K-TET Coaching</h2>
                                <div class="d-flex mb-4 text-center">
                                    <span class="h4 mb-0 mt-2">&#8377</span>
                                    <span class="price h1 mb-0 ">7999</span>
                                </div>
                                
                                <ul class="list-unstyled mb-0 pl-0">
                                    <li class="h6 text-muted mb-0"><span class="text-primary h5 mr-2"><i class="uim uim-check-circle"></i></span>Full Access</li>
                                    <li class="h6 text-muted mb-0"><span class="text-primary h5 mr-2"><i class="uim uim-check-circle"></i></span>Daily Classes</li>
                                    <li class="h6 text-muted mb-0"><span class="text-primary h5 mr-2"><i class="uim uim-check-circle"></i></span>Notes Proving</li>
                                    <li class="h6 text-muted mb-0"><span class="text-primary h5 mr-2"><i class="uim uim-check-circle"></i></span>Zoom Classes</li>
                                    <li class="h6 text-muted mb-0"><span class="text-primary h5 mr-2"><i class="uim uim-check-circle"></i></span>Learing</li>
                                </ul>
                                <a href="javascript:void(0)" class="btn btn-primary mt-4 disabled" style="pointer-events: none;background-color: #818181!important;
    border-color: #818181 !important;">Started Now</a>
                            </div>
                        </div>
                    </div><!--end col-->
                </div><!--end row-->
            </div><!--end container-->

            <div class="container-fluid mt-100 mt-60">
                <div class="rounded py-5" style="background: url({{ asset('frontend/assets/images/insurance/bg1.png') }}) center center;">
                    <div class="container py-md-5 py-3">
                        <div class="row">
                            <div class="col-lg-6 col-md-7 col-12 offset-lg-6 offset-md-5">
                                <div class="card border-0">
                                    <div class="card-body p-md-5 p-4 bg-white rounded">
                                        <div class="section-title">
                                            <h4 class="title mb-4">Advantages</h4>
                                            <p class="text-muted para-desc mb-0">Start connect with <span class="text-primary font-weight-bold">Future @ alert</span> that can provide everything you need to generate awareness, Guidence, Knowledge of students.</p>
                                        </div>
    
                                        <div class="row">
                                            <div class="col-md-6 col-12 mt-4">
                                                <div class="media align-items-center">
                                                    <div class="icon text-center rounded-circle h4 text-primary mr-2 mb-0">
                                                        <i class="uil uil-umbrella"></i>
                                                    </div>
                                                    <div class="media-body">
                                                        <h6 class="title text-dark mb-0">Protection</h6>
                                                    </div>
                                                </div>
                                            </div><!--end col-->
                                            
                                            <div class="col-md-6 col-12 mt-4">
                                                <div class="media align-items-center">
                                                    <div class="icon text-center rounded-circle h4 text-primary mr-2 mb-0">
                                                        <i class="uil uil-user"></i>
                                                    </div>
                                                    <div class="media-body">
                                                        <h6 class="title text-dark mb-0">Guidence</h6>
                                                    </div>
                                                </div>
                                            </div><!--end col-->
                                            
                                            <div class="col-md-6 col-12 mt-4">
                                                <div class="media align-items-center">
                                                    <div class="icon text-center rounded-circle h4 text-primary mr-2 mb-0">
                                                        <i class="uil uil-money-bill"></i>
                                                    </div>
                                                    <div class="media-body">
                                                        <h6 class="title text-dark mb-0">Support</h6>
                                                    </div>
                                                </div>
                                            </div><!--end col-->
                                            
                                            <div class="col-md-6 col-12 mt-4">
                                                <div class="media align-items-center">
                                                    <div class="icon text-center rounded-circle h4 text-primary mr-2 mb-0">
                                                        <i class="uil uil-bolt-alt"></i>
                                                    </div>
                                                    <div class="media-body">
                                                        <h6 class="title text-dark mb-0">Fast Process</h6>
                                                    </div>
                                                </div>
                                            </div><!--end col-->
                                        </div><!--end row-->
    
                                        <div class="mt-4">
                                            <a href="{{ route('contact-us')}}" class="btn btn-primary">Connect With Us<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right fea icon-sm"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg></a>
                                        </div>
                                    </div><!--end div-->
                                </div>
                            </div><!--end col-->
                        </div><!--end row-->
                    </div><!--end container-->
                </div><!--end div-->
            </div>

            <div class="container mt-100 mt-60">
                <div class="row justify-content-center">
                    <div class="col-12 text-center">
                        <div class="section-title mb-4 pb-2">
                            <h4 class="title mb-4">Schedule a demo with us</h4>
                            <p class="text-muted para-desc mx-auto mb-0">Start working with <span class="text-primary font-weight-bold">Future @ alert</span> that can provide everything you need to generate awareness, Guidence, connect.</p>
                        </div>
                    </div><!--end col-->
                </div><!--end row-->

                <div class="row justify-content-center mt-4">
                    <div class="col-lg-7 col-md-10">
                        <form>
                            <div class="form-group">
                                <div class="input-group mb-3">
                                    <input name="email" id="email" type="email" class="form-control" placeholder="Your email :" required="" aria-describedby="newssubscribebtn">
                                    <div class="input-group-append">
                                        <button class="btn btn-primary submitBnt" type="submit" id="newssubscribebtn">Subscribe</button>
                                    </div>
                                </div>
                            </div>
                        </form><!--end form-->
                    </div><!--end col-->
                </div><!--end row-->
            </div><!--end container-->
        </section><!--end section-->
        <!-- Price End -->

        <!-- Shape Start -->
        <div class="position-relative">
            <div class="shape overflow-hidden text-footer">
                <svg viewBox="0 0 2880 48" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M0 48H1437.5H2880V0H2160C1442.5 52 720 0 720 0H0V48Z" fill="currentColor"></path>
                </svg>
            </div>
        </div>
        <!--Shape End-->
@endsection
<script>
    $("#ElementName").css("pointer-events","none");
</script>
