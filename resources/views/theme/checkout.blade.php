@extends('theme.layouts.app')
@section('headerClass','')
@section('content')
      <!-- Hero Start -->
      <section class="bg-half bg-light d-table w-100">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-12 text-center">
                        <div class="page-next-level">
                            <h4 class="title"> Checkouts </h4>
                            <div class="page-next">
                                <nav aria-label="breadcrumb" class="d-inline-block">
                                    <ul class="breadcrumb bg-white rounded shadow mb-0">
                                        <li class="breadcrumb-item"><a href="{{route('/')}}">Home</a></li>
                                        <li class="breadcrumb-item active" aria-current="page">Checkouts</li>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </div><!--end col-->
                </div><!--end row-->
            </div> <!--end container-->
        </section><!--end section-->

        <div class="position-relative">
            <div class="shape overflow-hidden text-white">
                <svg viewBox="0 0 2880 48" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M0 48H1437.5H2880V0H2160C1442.5 52 720 0 720 0H0V48Z" fill="currentColor"></path>
                </svg>
            </div>
        </div>

        <section class="section vimeo-checkout-section">
            <div class="col-xl-6 col-lg-6 col-md-8 col-sm-8 mt-4 pt-2 mt-sm-0 pt-sm-0">
                <div class="position-relative">
                    <img src="images/busi01.jpg" class="rounded img-fluid mx-auto d-block" alt="">
                    <div class="play-icon">
                        <div style="padding:75% 0 0 0;position:relative;">
                            <iframe src="https://player.vimeo.com/video/649049744?h=3ebe6ed8af" frameborder="0" allow="autoplay; fullscreen" allowfullscreen style="position:absolute;top:0;left:0;width:100%;height:100%;"></iframe>
                        </div><script src="https://player.vimeo.com/api/player.js"></script>
                    </div>
                </div>
            </div>
        </section><!--end section-->

        <div class="container" style="text-align:center;font-size:20px;color:#2297da;">Watch to learn more about Edu Loan!</div>
        <!-- Hero End -->
        <!-- Start -->
        <section class="section">
            <div class="container">
                <div class="row">
                    <div class="col-lg-7 col-md-6">
                        <div class="rounded shadow-lg p-4">
                            <h5 class="mb-0">Billing Details :</h5>
                            <form class="mt-4" id="form" method="POST" action="{{url('payment')}}">@csrf
                                <div class="row">
                                    <div class="col-12">
                                        <div class="form-group position-relative">
                                            <label>Your Name <span class="text-danger">*</span></label>
                                            <input name="firstname" id="firstname" type="text" class="form-control" placeholder="First Name :" value ="{{ old('firstname') }}">
                                            @error('firstname')
                                                <span class="invalid-feedback d-block" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div><!--end col-->
                                    <div class="col-12">
                                        <div class="form-group position-relative">
                                            <label>Last Name <span class="text-danger">*</span></label>
                                            <input name="lastname" id="lastname" type="text" class="form-control" placeholder="Last Name :" value ="{{ old('lastname') }}">
                                            @error('lastname')
                                                <span class="invalid-feedback d-block" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div><!--end col-->
                                    <div class="col-12">
                                        <div class="form-group position-relative">
                                            <label>Street address <span class="text-danger">*</span></label>
                                            <input type="text" name="street" id="street" class="form-control" placeholder="House number and street name :" value ="{{ old('street') }}">
                                            @error('street')
                                                <span class="invalid-feedback d-block" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div><!--end col-->
                                    <div class="col-12">
                                        <div class="form-group position-relative">
                                            <label>Apartment, suite, unit etc. <span class="text-muted">(Optional)</span></label>
                                            <input type="text" name="address2" id="address2" class="form-control" placeholder="Apartment, suite, unit etc. :" value ="{{ old('address2') }}">
                                        </div>
                                    </div><!--end col-->
                                    <div class="col-md-6">
                                        <div class="form-group position-relative">
                                            <label>Town / City <span class="text-danger">*</span></label>
                                            <input type="text" name="city" id="city" class="form-control" placeholder="City Name :" value ="{{ old('city') }}">
                                            @error('city')
                                                <span class="invalid-feedback d-block" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div><!--end col-->
                                    <div class="col-md-6">
                                        <div class="form-group position-relative">
                                            <label>Postal Code <span class="text-danger">*</span></label>
                                            <input type="text" name="postcode" id="postcode" class="form-control" placeholder="Zip :" value ="{{ old('postcode') }}">
                                            @error('postcode')
                                                <span class="invalid-feedback d-block" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div><!--end col-->
                                    <div class="col-md-6">
                                        <div class="form-group position-relative">
                                            <label>State <span class="text-danger">*</span></label>
                                            <input type="text" name="state" id="state" class="form-control" placeholder="State Name :" value ="{{ old('state') }}">
                                            @error('state')
                                                <span class="invalid-feedback d-block" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div><!--end col--> 
                                    <div class="col-12">
                                        <div class="form-group position-relative">
                                            <label>Phone <span class="text-danger">*</span></label>
                                            <input type="text" name="phone" id="phone" class="form-control" placeholder="Phone number :" value ="{{ old('phone') }}">
                                            @error('phone')
                                                <span class="invalid-feedback d-block" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div><!--end col-->
                                    <div class="col-12">
                                        <div class="form-group position-relative">
                                            <label>Your Email <span class="text-danger">*</span></label>
                                            <input name="email" id="email" type="email" class="form-control" placeholder="Your email :" value ="{{ old('email') }}">
                                            @error('email')
                                                <span class="invalid-feedback d-block" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div> 
                                    </div><!--end col-->
                                    <div class="col-12">
                                        <div class="mt-4 pt-2">
                                            <!-- <a href="shop-checkouts.html" class="btn btn-block btn-primary"></a> -->
                                            <button class="btn btn-block btn-primary" id="js-customer-details">Continue</button>
                                        </div>
                                    </div>
                                    <input type="hidden" name="register_fee" id="register_fee" value="999" >
                                </div><!--end row-->
                            </form><!--end form-->
                        </div>
                    </div><!--end col-->

                    <div class="col-lg-5 col-md-6 mt-4 mt-sm-0 pt-2 pt-sm-0">
                        <div class="rounded shadow-lg p-4">
                            <div class="table-responsive">
                                <table class="table table-center table-padding mb-0">
                                    <tbody>
                                        <tr>
                                            <td class="h6 border-0">Subtotal</td>
                                            <td class="text-center font-weight-bold border-0">&#8377; 999/-</td>
                                        </tr>
                                        <tr class="bg-light">
                                            <td class="h5 font-weight-bold">Total</td>
                                            <td class="text-center text-primary h4 font-weight-bold">&#8377; 999/-</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div><!--end row-->
            </div><!--end container-->
        </section><!--end section-->
        <!-- End -->
@endsection

