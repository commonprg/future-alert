
<!DOCTYPE html>
    <html lang="en">

    <head>
        <meta charset="utf-8" />
        <title>Future @ ALert</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="Version" content="v2.5.1" />
       <!-- favicon -->
       <link rel="shortcut icon" href="{{ asset('frontend/assets/images/favicon.ico') }}"> 
        <!-- Bootstrap -->
        <link href="{{ asset('frontend/assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
        <!-- Icons -->
        <link href="{{ asset('frontend/assets/css/materialdesignicons.min.css') }}" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="https://unicons.iconscout.com/release/v2.1.9/css/unicons.css">
        <!-- Slider -->               
        <link rel="stylesheet" href="{{ asset('frontend/assets/css/owl.carousel.min.css') }}"/> 
        <link rel="stylesheet" href="{{ asset('frontend/assets/css/owl.theme.default.min.css') }}"/> 
        <!-- Main Css -->
        <link href="{{ asset('frontend/assets/css/style.css') }}" rel="stylesheet" type="text/css" id="theme-opt" />
        <link href="{{ asset('frontend/assets/css/colors/default.css') }}" rel="stylesheet" id="color-opt">

    </head>

    <body>
        <!-- Loader -->
        <!-- <div id="preloader">
            <div id="status">
                <div class="spinner">
                    <div class="double-bounce1"></div>
                    <div class="double-bounce2"></div>
                </div>
            </div>
        </div> -->
        <!-- Loader -->
        
        <div class="back-to-home rounded d-none d-sm-block">
            <a href="{{ route('/') }}" class="btn btn-icon btn-soft-primary"><i data-feather="home" class="icons"></i></a>
        </div>

        <!-- Hero Start -->
        <section class="bg-home d-flex align-items-center">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-7 col-md-6">
                        <div class="mr-lg-5">   
                            <img src="{{ asset('frontend/assets/images/user/recovery.svg') }}" class="img-fluid d-block mx-auto" alt="">
                        </div>
                    </div>
                    <div class="col-lg-5 col-md-6">
                        <div class="card login_page shadow rounded border-0">
                            
                            <div class="card-body">
                                <h4 class="card-title text-center">Recover Account</h4> 

                                <form class="login-form mt-4" method="POST" action="{{ route('password.email') }}">
                                @csrf
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <p class="text-muted">Please enter your email address. You will receive a link to create a new password via email.</p>
                                            <div class="form-group position-relative">
                                                <label>Email address <span class="text-danger">*</span></label>
                                                <i data-feather="mail" class="fea icon-sm icons"></i>
                                                <input type="email" id="email" class="form-control pl-5" placeholder="Enter Your Email Address" name="email" required="">
                                            </div>
                                            @if (session('status'))
                                                <div class="alert alert-success" role="alert">
                                                    {{ session('status') }} 
                                            @endif
                                        </div>
                                        <div class="col-lg-12">
                                            <button class="btn btn-primary btn-block">{{ __('Email Password Reset Link') }}</button>
                                        </div>
                                        <div class="mx-auto">
                                            <p class="mb-0 mt-3"><small class="text-dark mr-2">Remember your password ?</small> <a href="{{ route('login') }}" class="text-dark font-weight-bold">Sign in</a></p>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div> <!--end col-->
                </div><!--end row-->
            </div> <!--end container-->
        </section><!--end section-->
        <!-- Hero End -->

        <!-- javascript -->
        <script src="{{ asset('frontend/assets/js/jquery-3.5.1.min.js') }}"></script>
        <script src="{{ asset('frontend/assets/js/bootstrap.bundle.min.js') }}"></script>
        <script src="{{ asset('frontend/assets/js/jquery.easing.min.js') }}"></script>
        <script src="{{ asset('frontend/assets/js/scrollspy.min.js') }}"></script>
        <!-- Icons -->
        <script src="{{ asset('frontend/assets/js/feather.min.js') }}"></script>
        <script src="https://unicons.iconscout.com/release/v2.1.9/script/monochrome/bundle.js"></script>
        <!-- Main Js -->
        <script src="{{ asset('frontend/assets/js/app.js') }}"></script>
    </body>
</html>