@extends('theme.layouts.app')
@section('headerClass','')
@section('content')
<!-- Hero Start -->
    <section class="bg-half bg-light d-table w-100" >
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-12 text-center">
                    <div class="page-next-level">
                        <h4 class="title"> Pricing </h4>
                    </div>
                </div>  <!--end col-->
            </div><!--end row-->
        </div> <!--end container-->
    </section><!--end section-->
    <!-- Hero End -->

    <!-- Shape Start -->
    <div class="position-relative">
        <div class="shape overflow-hidden text-white">
            <svg viewBox="0 0 2880 48" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M0 48H1437.5H2880V0H2160C1442.5 52 720 0 720 0H0V48Z" fill="currentColor"></path>
            </svg>
        </div>
    </div>
    <!--Shape End-->

    <!-- Price Start -->
    <section class="section">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12 text-center">
                    <div class="section-title mb-4 pb-2">
                        <h4 class="title mb-4">EDU- LOAN ?.... CSR SCOLARSHIP.?</h4>
                        <p class="text-muted para-desc mb-0 mx-auto">Introducing a new initiate which is very helpful for higher education seekers for submitting their eduloan & C S R Scholarship application through online..</p>
                    </div>
                </div><!--end col-->
            </div><!--end row-->

            <div class="row align-items-center justify-content-center">
                
            <div class="col-lg-3 col-md-6 col-12 mt-4 pt-2">
                        <div class="card pricing-rates business-rate shadow bg-light border-0 rounded">
                            <div class="card-body">
                                <h2 class="title text-uppercase mb-4">Edu-loan Class</h2>
                                <div class="d-flex mb-0 text-center" style="height:45px">
                                    <span class="h4 mb-0 mt-2">&#8377</span>
                                    <del style="color:#373738;"><span class="price h1 mb-0" style="color:red;">2999</span></del>&nbsp;&nbsp;&nbsp;
                                    <span class="h6 mb-0 mt-2" style="color:green">&#8377</span>
                                    <span class="price h3 mb-0" style="color:green">999</span>
                                </div>
                                <span class="h6 mt-2 offer-price" style="color:red">Save <span class="h6 mb-0 mt-2" style="color:red">&#8377</span>2000</span>

                                <ul class="list-unstyled mb-0 mt-3 pl-0">
                                    <li class="h6 text-muted mb-0"><span class="text-primary h5 mr-2"><i class="uim uim-check-circle"></i></span>Full Access</li>
                                    <li class="h6 text-muted mb-0"><span class="text-primary h5 mr-2"><i class="uim uim-check-circle"></i></span>Daily Classes</li>
                                    <li class="h6 text-muted mb-0"><span class="text-primary h5 mr-2"><i class="uim uim-check-circle"></i></span>Source File</li>
                                    <li class="h6 text-muted mb-0"><span class="text-primary h5 mr-2"><i class="uim uim-check-circle"></i></span>Zoom Classes</li>
                                    <li class="h6 text-muted mb-0"><span class="text-primary h5 mr-2"><i class="uim uim-check-circle"></i></span>Learing</li>
                                </ul>
                                <a href="{{ route('take-course') }}" class="btn btn-primary mt-4">Started Now</a>
                            </div>
                        </div>
                    </div><!--end col-->
                    <div class="col-lg-3 col-md-6 col-12 mt-4 pt-2">
                        <div class="card pricing-rates business-rate shadow bg-light border-0 rounded">
                            <div class="card-body">
                                <h2 class="title text-uppercase mb-4">PSC Coaching</h2>
                                <div class="d-flex mb-4 text-center">
                                    <span class="h4 mb-0 mt-2">&#8377</span>
                                    <span class="price h1 mb-0 ">4999</span>
                                </div>
                                
                                <ul class="list-unstyled mb-0 pl-0">
                                    <li class="h6 text-muted mb-0"><span class="text-primary h5 mr-2"><i class="uim uim-check-circle"></i></span>Full Access</li>
                                    <li class="h6 text-muted mb-0"><span class="text-primary h5 mr-2"><i class="uim uim-check-circle"></i></span>Daily Classes</li>
                                    <li class="h6 text-muted mb-0"><span class="text-primary h5 mr-2"><i class="uim uim-check-circle"></i></span>Notes Proving</li>
                                    <li class="h6 text-muted mb-0"><span class="text-primary h5 mr-2"><i class="uim uim-check-circle"></i></span>Zoom Classes</li>
                                    <li class="h6 text-muted mb-0"><span class="text-primary h5 mr-2"><i class="uim uim-check-circle"></i></span>Learing</li>
                                </ul>
                                <a href="javascript:void(0)" class="btn btn-primary mt-4" style="pointer-events: none;background-color: #818181!important;
    border-color: #818181 !important;">Started Now</a>
                            </div>
                        </div>
                    </div><!--end col-->
                    <div class="col-lg-3 col-md-6 col-12 mt-4 pt-2">
                        <div class="card pricing-rates business-rate shadow bg-light border-0 rounded">
                            <div class="card-body">
                                <h2 class="title text-uppercase mb-4">K-TET Coaching</h2>
                                <div class="d-flex mb-4 text-center">
                                    <span class="h4 mb-0 mt-2">&#8377</span>
                                    <span class="price h1 mb-0 ">7999</span>
                                </div>
                                
                                <ul class="list-unstyled mb-0 pl-0">
                                    <li class="h6 text-muted mb-0"><span class="text-primary h5 mr-2"><i class="uim uim-check-circle"></i></span>Full Access</li>
                                    <li class="h6 text-muted mb-0"><span class="text-primary h5 mr-2"><i class="uim uim-check-circle"></i></span>Daily Classes</li>
                                    <li class="h6 text-muted mb-0"><span class="text-primary h5 mr-2"><i class="uim uim-check-circle"></i></span>Notes Proving</li>
                                    <li class="h6 text-muted mb-0"><span class="text-primary h5 mr-2"><i class="uim uim-check-circle"></i></span>Zoom Classes</li>
                                    <li class="h6 text-muted mb-0"><span class="text-primary h5 mr-2"><i class="uim uim-check-circle"></i></span>Learing</li>
                                </ul>
                                <a href="javascript:void(0)" class="btn btn-primary mt-4" style="pointer-events: none;background-color: #818181!important;
    border-color: #818181 !important;">Started Now</a>
                            </div>
                        </div><!--end col-->
                
                
            </div><!--end row-->
        </div><!--end container-->

        <!-- Price End -->

  
    </section><!--end section-->
    <!-- Testi End -->

    <!-- FAQ n Contact Start -->
    <section class="section bg-light">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-12">
                    <div class="media">
                        <i data-feather="help-circle" class="fea icon-ex-md text-primary mr-2 mt-1"></i>
                        <div class="media-body">
                            <h5 class="mt-0">How our <span class="text-primary">Future @ alert</span> work ?</h5>
                            <p class="answer text-muted mb-0">We help and support of all languages in india to increase visibility ,efficiency,drive more scholarship and improve proffessional educational success. Guiding students a solution to their financial needs.</p>
                        </div>
                    </div>
                </div><!--end col-->
                
                <div class="col-md-6 col-12 mt-4 mt-sm-0 pt-2 pt-sm-0">
                    <div class="media">
                        <i data-feather="help-circle" class="fea icon-ex-md text-primary mr-2 mt-1"></i>
                        <div class="media-body">
                            <h5 class="mt-0"> What is the main process open account ?</h5>
                            <p class="answer text-muted mb-0">Dedicated and intensive online coaching forsubmitting edu-load applicationand scholarship application through online</p>
                        </div>
                    </div>
                </div><!--end col-->
                
                <div class="col-md-6 col-12 mt-4 pt-2">
                    <div class="media">
                        <i data-feather="help-circle" class="fea icon-ex-md text-primary mr-2 mt-1"></i>
                        <div class="media-body">
                            <h5 class="mt-0"> How to make Safe Guarding student's Right</h5>
                            <p class="answer text-muted mb-0">Drafting various complaints & processing applications unde RTI Act 2005,which will enable every students to safeguard their Rights.</p>
                        </div>
                    </div>
                </div><!--end col-->
                
                <div class="col-md-6 col-12 mt-4 pt-2">
                    <div class="media">
                        <i data-feather="help-circle" class="fea icon-ex-md text-primary mr-2 mt-1"></i>
                        <div class="media-body">
                            <h5 class="mt-0"> What we introducing</h5>
                            <p class="answer text-muted mb-0">Introducing a new initiate which is very helpful for higher education seekers for submitting their eduloan & C S R Scholarship application through online.</p>
                        </div>
                    </div>
                </div><!--end col-->
            </div><!--end row-->

            <div class="row mt-md-5 pt-md-3 mt-4 pt-2 mt-sm-0 pt-sm-0 justify-content-center">
                <div class="col-12 text-center">
                    <div class="section-title">
                        <h4 class="title mb-4">Have Question ? Get in touch!</h4>
                        <p class="text-muted para-desc mx-auto">Start working with <span class="text-primary font-weight-bold">Future Alert</span> that can provide everything you need to generate awareness, guidence.</p>
                        <div class="mt-4 pt-2">
                            <a href="{{ route('contact-us')}}" class="btn btn-primary">Contact us <i class="mdi mdi-arrow-right"></i></a>
                        </div>
                    </div>
                </div><!--end col-->
            </div><!--end row-->
        </div><!--end container-->
    </section><!--end section-->
    <!-- FAQ n Contact End -->
        @endsection