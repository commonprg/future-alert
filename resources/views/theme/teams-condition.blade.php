@extends('theme.layouts.app')
@section('headerClass','')
@section('content')
<section class="bg-half bg-light d-table w-100">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-12 text-center">
                <div class="page-next-level">
                    <h4 class="title"> Terms of Services </h4>
                </div>
            </div>  <!--end col-->
        </div><!--end row-->
    </div> <!--end container-->
</section><!--end section-->
<!-- Hero End -->

<!-- Shape Start -->
<div class="position-relative">
    <div class="shape overflow-hidden text-white">
        <svg viewBox="0 0 2880 48" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M0 48H1437.5H2880V0H2160C1442.5 52 720 0 720 0H0V48Z" fill="currentColor"></path>
        </svg>
    </div>
</div>
<!--Shape End-->  

<!-- Start Terms & Conditions -->
<section class="section">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-9">
                <div class="card shadow border-0 rounded">
                    <div class="card-body">
                        <h5 class="card-title">Introduction :</h5>
                        <p class="text-muted">Welcome to falert.in!.</p>
                        <p class="text-muted">These terms and conditions outline the rules and regulations for the use of Future alert's Website, located at http://falert.in/.</p>
                        <p class="text-muted">By accessing this website we assume you accept these terms and conditions. Do not continue to use falert.in if you do not agree to take all of the terms and conditions stated on this page.</p>
                        <p class="text-muted">The following terminology applies to these Terms and Conditions, Privacy Statement and Disclaimer Notice and all Agreements: Your refers to you, the person log on this website and compliant to the Company’s terms and conditions.We refers to our Company. Parties refers to both the Client and ourselves. All terms refer to the offer, acceptance and consideration of payment necessary to undertake the process of our assistance to the Client in the most appropriate manner for the express purpose of meeting the Client’s needs in respect of provision of the Company’s stated services, in accordance with and subject to, prevailing law of india. Any use of the above terminology or other words in the singular, plural, capitalization and/or he/she or they, are taken as interchangeable and therefore as referring to same.</p>

                        <h5 class="card-title">Users Question & Answer :</h5>
                        <div class="faq-content mt-4">
                            <div class="accordion" id="accordionExampleone">
                                <div class="card border-0 rounded mb-2">
                                    <a data-toggle="collapse" href="#collapseone" class="faq position-relative" aria-expanded="true" aria-controls="collapseone">
                                        <div class="card-header border-0 bg-light p-3 pr-5" id="headingfifone">
                                            <h6 class="title mb-0">Cookies</h6>
                                        </div>
                                    </a>
                                    <div id="collapseone" class="collapse show" aria-labelledby="headingfifone" data-parent="#accordionExampleone">
                                        <div class="card-body px-2 py-4">
                                            <p class="text-muted mb-0 faq-ans">We employ the use of cookies. By accessing falert.in, you agreed to use cookies in agreement with the Future alert's Privacy Policy.</p>
                                            <p class="text-muted mb-0 faq-ans">Most interactive websites use cookies to let us retrieve the user’s details for each visit. Cookies are used by our website to enable the functionality of certain areas to make it easier for people visiting our website. Some of our affiliate/advertising partners may also use cookies.</p>
                                        </div>
                                    </div>
                                </div>

                                <div class="card border-0 rounded mb-2">
                                    <a data-toggle="collapse" href="#collapsetwo" class="faq position-relative collapsed" aria-expanded="false" aria-controls="collapsetwo">
                                        <div class="card-header border-0 bg-light p-3 pr-5" id="headingtwo">
                                            <h6 class="title mb-0">Licence</h6>
                                        </div>
                                    </a>
                                    <div id="collapsetwo" class="collapse" aria-labelledby="headingtwo" data-parent="#accordionExampleone">
                                        <div class="card-body px-2 py-4">
                                            <p class="text-muted mb-0 faq-ans">Unless otherwise stated, Future alert and/or its licensors own the intellectual property rights for all material on falert.in. All intellectual property rights are reserved. You may access this from falert.in for your own personal use subjected to restrictions set in these terms and conditions.</p>
                                            <p class="text-muted mb-0 faq-ans">You must not:</p>
                                            <ul class="list-unstyled text-muted">
                                                <li><i data-feather="arrow-right" class="fea icon-sm mr-2"></i>Republish material from falert.in</li>
                                                <li><i data-feather="arrow-right" class="fea icon-sm mr-2"></i>Sell, rent or sub-license material from falert.in</li>
                                                <li><i data-feather="arrow-right" class="fea icon-sm mr-2"></i>Reproduce, duplicate or copy material from falert.in</li>
                                                <li><i data-feather="arrow-right" class="fea icon-sm mr-2"></i>Redistribute content from falert.in</li>
                                            </ul>
                                            <p class="text-muted mb-0 faq-ans">This Agreement shall begin on the date hereof. Our Terms and Conditions were created with the help of the Terms And Conditions Generator</p>
                                            <p class="text-muted mb-0 faq-ans">Parts of this website offer an opportunity for users to post and exchange opinions and information in certain areas of the website. Future alert does not filter, edit, publish or review Comments prior to their presence on the website. Comments do not reflect the views and opinions of Future alert,its agents and/or affiliates. Comments reflect the views and opinions of the person who post their views and opinions. To the extent permitted by applicable laws, Future alert shall not be liable for the Comments or for any liability, damages or expenses caused and/or suffered as a result of any use of and/or posting of and/or appearance of the Comments on this website.</p>
                                            <p class="text-muted mb-0 faq-ans">Future alert reserves the right to monitor all Comments and to remove any Comments which can be considered inappropriate, offensive or causes breach of these Terms and Conditions.</p>
                                            <p class="text-muted mb-0 faq-ans">You warrant and represent that:</p>
                                            <ul class="list-unstyled text-muted">
                                                <li><i data-feather="arrow-right" class="fea icon-sm mr-2"></i>You are entitled to post the Comments on our website and have all necessary licenses and consents to do so</li>
                                                <li><i data-feather="arrow-right" class="fea icon-sm mr-2"></i>The Comments do not invade any intellectual property right, including without limitation copyright, patent or trademark of any third party</li>
                                                <li><i data-feather="arrow-right" class="fea icon-sm mr-2"></i>The Comments do not contain any defamatory, libelous, offensive, indecent or otherwise unlawful material which is an invasion of privacy</li>
                                                <li><i data-feather="arrow-right" class="fea icon-sm mr-2"></i>The Comments will not be used to solicit or promote business or custom or present commercial activities or unlawful activity</li>
                                            </ul>
                                            <p class="text-muted mb-0 faq-ans">You hereby grant Future alert a non-exclusive license to use, reproduce, edit and authorize others to use, reproduce and edit any of your Comments in any and all forms, formats or media</p>
                                        </div>
                                    </div>
                                </div>

                                <div class="card border-0 rounded mb-2">
                                    <a data-toggle="collapse" href="#collapsethree" class="faq position-relative collapsed" aria-expanded="false" aria-controls="collapsethree">
                                        <div class="card-header border-0 bg-light p-3 pr-5" id="headingthree">
                                            <h6 class="title mb-0">iFrames</h6>
                                        </div>
                                    </a>
                                    <div id="collapsethree" class="collapse" aria-labelledby="headingthree" data-parent="#accordionExampleone">
                                        <div class="card-body px-2 py-4">
                                            <p class="text-muted mb-0 faq-ans">Without prior approval and written permission, you may not create frames around our Webpages that alter in any way the visual presentation or appearance of our Website.</p>
                                        </div>
                                    </div>
                                </div>

                                <div class="card border-0 rounded mb-2">
                                    <a data-toggle="collapse" href="#collapsefour" class="faq position-relative collapsed" aria-expanded="false" aria-controls="collapsefour">
                                        <div class="card-header border-0 bg-light p-3 pr-5" id="headingfour">
                                            <h6 class="title mb-0">Content Liability</h6>
                                        </div>
                                    </a>
                                    <div id="collapsefour" class="collapse" aria-labelledby="headingfour" data-parent="#accordionExampleone">
                                        <div class="card-body px-2 py-4">
                                            <p class="text-muted mb-0 faq-ans">We shall not be hold responsible for any content that appears on your Website. You agree to protect and defend us against all claims that is rising on your Website. No link(s) should appear on any Website that may be interpreted as libelous, obscene or criminal, or which infringes, otherwise violates, or advocates the infringement or other violation of, any third party rights.</p>
                                        </div>
                                    </div>
                                </div>

                                <div class="card border-0 rounded mb-2">
                                    <a data-toggle="collapse" href="#collapsefive2" class="faq position-relative collapsed" aria-expanded="false" aria-controls="collapsefive2">
                                        <div class="card-header border-0 bg-light p-3 pr-5" id="headingfive2">
                                            <h6 class="title mb-0"> Your Privacy ? </h6>
                                        </div>
                                    </a>
                                    <div id="collapsefive2" class="collapse" aria-labelledby="headingfive2" data-parent="#accordionExampleone">
                                        <div class="card-body px-2 py-4">
                                            <p class="text-muted mb-0 faq-ans">Please read Privacy Policy.</p>
                                        </div>
                                    </div>
                                </div>

                                <div class="card border-0 rounded mb-2">
                                    <a data-toggle="collapse" href="#collapsefive3" class="faq position-relative collapsed" aria-expanded="false" aria-controls="collapsefive3">
                                        <div class="card-header border-0 bg-light p-3 pr-5" id="headingfive2">
                                            <h6 class="title mb-0"> Reservation of Rights </h6>
                                        </div>
                                    </a>
                                    <div id="collapsefive3" class="collapse" aria-labelledby="headingfive2" data-parent="#accordionExampleone">
                                        <div class="card-body px-2 py-4">
                                            <p class="text-muted mb-0 faq-ans">We reserve the right to request that you remove all links or any particular link to our Website. You approve to immediately remove all links to our Website upon request. We also reserve the right to amen these terms and conditions and it’s linking policy at any time. By continuously linking to our Website, you agree to be bound to and follow these linking terms and conditions.</p>
                                        </div>
                                    </div>
                                </div>

                                <div class="card border-0 rounded mb-2">
                                    <a data-toggle="collapse" href="#collapsefive4" class="faq position-relative collapsed" aria-expanded="false" aria-controls="collapsefive4">
                                        <div class="card-header border-0 bg-light p-3 pr-5" id="headingfive2">
                                            <h6 class="title mb-0"> Disclaimer </h6>
                                        </div>
                                    </a>
                                    <div id="collapsefive4" class="collapse" aria-labelledby="headingfive2" data-parent="#accordionExampleone">
                                        <div class="card-body px-2 py-4">
                                            <p class="text-muted mb-0 faq-ans">To the maximum extent permitted by applicable law, we exclude all representations, warranties and conditions relating to our website and the use of this website. Nothing in this disclaimer will:</p>
                                            <ul class="list-unstyled text-muted">
                                                <li><i data-feather="arrow-right" class="fea icon-sm mr-2"></i>limit or exclude our or your liability for death or personal injury;</li>
                                                <li><i data-feather="arrow-right" class="fea icon-sm mr-2"></i>limit or exclude our or your liability for fraud or fraudulent misrepresentation;</li>
                                                <li><i data-feather="arrow-right" class="fea icon-sm mr-2"></i>limit any of our or your liabilities in any way that is not permitted under applicable law; or</li>
                                                <li><i data-feather="arrow-right" class="fea icon-sm mr-2"></i>exclude any of our or your liabilities that may not be excluded under applicable law.</li>
                                            </ul>
                                            <p class="text-muted mb-0 faq-ans">The limitations and prohibitions of liability set in this Section and elsewhere in this disclaimer: (a) are subject to the preceding paragraph; and (b) govern all liabilities arising under the disclaimer, including liabilities arising in contract, in tort and for breach of statutory duty</p>
                                            <p class="text-muted mb-0 faq-ans">As long as the website and the information and services on the website are provided free of charge, we will not be liable for any loss or damage of any nature</p>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div class="mt-3">
                            <a href="javascript:void(0)" class="btn btn-primary mt-2 mr-2">Accept</a>
                            <a href="javascript:void(0)" class="btn btn-outline-primary mt-2">Decline</a>
                        </div>
                    </div>
                </div>
            </div><!--end col-->
        </div><!--end row-->
    </div><!--end container-->
</section><!--end section-->
<!-- End Terms & Conditions -->
@endsection