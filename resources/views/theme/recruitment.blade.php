@extends('theme.layouts.app')
@section('headerClass','')
@section('content')
 <!-- Start -->
 <!-- Hero Start -->
 <section class="bg-half bg-light d-table w-100">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-12 text-center">
                        <div class="page-next-level">
                            <h4 class="title"> Nursing Application </h4>
                        </div>
                    </div><!--end col-->
                </div><!--end row-->
            </div> <!--end container-->
        </section><!--end section-->
        <div class="position-relative">
            <div class="shape overflow-hidden text-white">
                <svg viewBox="0 0 2880 48" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M0 48H1437.5H2880V0H2160C1442.5 52 720 0 720 0H0V48Z" fill="currentColor"></path>
                </svg>
            </div>
        </div>
        <!-- Hero End -->
<section class="section">
    <div class="container">
        <div class="col">
            <div class="rounded shadow-lg p-4">
                <form class="mt-4" id="form" method="POST" action="{{url('recruit')}}" enctype="multipart/form-data">@csrf
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group position-relative">
                                <label>First Name <span class="text-danger">*</span></label>
                                <input name="firstname" id="firstname" type="text" class="form-control" placeholder="First Name :" value ="{{ old('firstname') }}">
                                @error('firstname')
                                    <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div><!--end col-->
                        <div class="col-6">
                            <div class="form-group position-relative">
                                <label>Last Name <span class="text-danger">*</span></label>
                                <input name="lastname" id="lastname" type="text" class="form-control" placeholder="Last Name :" value ="{{ old('lastname') }}">
                                @error('lastname')
                                    <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div><!--end col-->
                        <div class="col-12">
                            <div class="form-group position-relative">
                                <label>Street address <span class="text-danger">*</span></label>
                                <input type="text" name="street" id="street" class="form-control" placeholder="House number and street name :" value ="{{ old('street') }}">
                                @error('street')
                                    <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div><!--end col-->
                        <div class="col-12">
                            <div class="form-group position-relative">
                                <label>Apartment, suite, unit etc. <span class="text-muted">(Optional)</span></label>
                                <input type="text" name="address2" id="address2" class="form-control" placeholder="Apartment, suite, unit etc. :" value ="{{ old('address2') }}">
                            </div>
                        </div><!--end col-->
                        <div class="col-md-6">
                            <div class="form-group position-relative">
                                <label>Town / City <span class="text-danger">*</span></label>
                                <input type="text" name="city" id="city" class="form-control" placeholder="City Name :" value ="{{ old('city') }}">
                                @error('city')
                                    <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div><!--end col-->
                        <!-- <div class="col-md-6">
                            <div class="form-group position-relative">
                                <label>Address <span class="text-danger">*</span></label>
                                <textarea name="Address" id="Address" class="form-control" placeholder="Address :">{{ old('Address') }}</textarea>
                                @error('Address')
                                    <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div> -->
                        <div class="col-md-6">
                            <div class="form-group position-relative">
                                <label>State <span class="text-danger">*</span></label>
                                <input type="text" name="state" id="state" class="form-control" placeholder="State Name :" value ="{{ old('state') }}">
                                @error('state')
                                    <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-group position-relative">
                                <label>Postal Code <span class="text-danger">*</span></label>
                                <input type="text" name="postcode" id="postcode" class="form-control" placeholder="Zip :" value ="{{ old('postcode') }}">
                                @error('postcode')
                                    <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div><!--end col--> 
                        <div class="col-6">
                            <div class="form-group position-relative">
                                <label>Phone <span class="text-danger">*</span></label>
                                <input type="text" name="phone" id="phone" class="form-control" placeholder="Phone number :" value ="{{ old('phone') }}">
                                @error('phone')
                                    <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div><!--end col-->
                        <div class="col-6">
                            <div class="form-group position-relative">
                                <label>Your Email <span class="text-danger">*</span></label>
                                <input name="email" id="email" type="email" class="form-control" placeholder="Your email :" value ="{{ old('email') }}">
                                @error('email')
                                    <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div> 
                        </div><!--end col-->
                        <div class="col-6">
                            <div class="form-group position-relative">
                                <label>Nursing Enrollment Registration Number <span class="text-danger">*</span></label>
                                <input name="regNo" id="regNo" type="regNo" class="form-control" placeholder="Your Register Number :" value ="{{ old('regNo') }}">
                                @error('regNo')
                                    <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div> 
                        </div><!--end col-->
                        <div class="col-6">
                            <div class="form-group position-relative">
                                <label>Year of Experience <span class="text-danger">*</span></label>
                                <input name="yearofExp" id="yearofExp" type="yearofExp" class="form-control" placeholder="Year of Experience :" value ="{{ old('yearofExp') }}">
                                @error('yearofExp')
                                    <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div> 
                        </div><!--end col-->
                        <div class="col-6">
                            <div class="form-group">
                                <label for="document">Upload Experience Letter:</label>
                                <input type="file" name="document" id="document" class="form-control" placeholder="Year of Experience :" value="{{old('document')}}" accept="application/pdf">
                                @error('document')
                                    <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div><!--end col-->
                        <div class="col-6">
                            <div class="form-group">
                                <label for="image">Upload Profile Photo:</label>
                                <input type="file" name="image" id="image" class="form-control" placeholder="Year of Experience :" value="{{old('image')}}" accept="image/png, image/jpg, image/jpeg">
                                @error('image')
                                    <span class="invalid-feedback d-block" role="alert" accept="image/*">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div><!--end col-->
                        <div class="col-12">
                            <div class="mt-4 pt-2" style="padding-right:400px;padding-left:400px;">
                                <button class="btn btn-block btn-primary" id="js-customer-details">Apply</button>
                            </div>
                        </div>
                        
                    </div><!--end row-->
                </form><!--end form-->
            </div>
        </div><!--end col-->
    </div><!--end container-->
</section><!--end section-->
<!-- End -->            
@endsection