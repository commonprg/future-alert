@extends('theme.layouts.app')
@section('headerClass','')
@section('content')
<!-- Hero Start -->
<section class="bg-half bg-light d-table w-100">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-12 text-center">
                        <div class="page-next-level">
                            <h4 class="title"> Aboutus </h1>
                            <p class="text-white-50 para-desc mb-0 mx-auto">Start working with Landrick that can provide everything you need to generate awareness, drive traffic, connect.</p>
                        </div>
                        <div class="page-next">
                            <nav aria-label="breadcrumb" class="d-inline-block">
                                <ul class="breadcrumb bg-white rounded shadow mb-0">
                                    @foreach (config('language') as $lang => $language)
                                        <li class="breadcrumb-item active"  aria-current="page"><a href="{{ route('lang.switch', $lang) }}">{{$language}}</a></li>
                                    @endforeach
                                </ul>
                            </nav>
                        </div>
                    </div><!--end col-->
                    
                </div><!--end row--> 
            </div> <!--end container-->
        </section><!--end section-->
        
        <div class="position-relative">
            <div class="shape overflow-hidden text-white">
                <svg viewBox="0 0 2880 48" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M0 48H1437.5H2880V0H2160C1442.5 52 720 0 720 0H0V48Z" fill="currentColor"></path>
                </svg>
            </div>
        </div>
        <!-- Hero End -->

        <section class="section">
            <div class="container">
                <div class="row align-items-center" id="counter">
                    <div class="col-md-6">
                        <img src="{{ asset('frontend/assets/images/company/about2.png') }}" class="img-fluid" alt="">
                        <img src="{{ asset('frontend/assets/images/company/test01.png') }}" class="img-fluid" alt="">
                    </div><!--end col-->

                    <div class="col-md-6 mt-4 pt-2 mt-sm-0 pt-sm-0">
                        <div class="ml-lg-4">
                            <div class="section-title">
                                <h4 class="title mb-4">Who we are ?</h4>
                                <p class="text-muted">{{__('messages.top-para')}}</p>
                                <p class="text-muted">{{__('messages.top-para-sub')}}</p>
                                <p class="text-muted">{{__('messages.top-para-sub-1')}}</p>
                                <p class="text-muted">{{__('messages.ui-para')}}</p>
                                <ul class="list-unstyled mt-4 mb-0 pl-2">
                                    <li class="mt-2"><i class="mdi mdi-arrow-right text-primary mr-1"></i>{{__('messages.option-1')}}</li>
                                    <li class="mt-2"><i class="mdi mdi-arrow-right text-primary mr-1"></i>{{__('messages.option-2')}}</li>
                                    <li class="mt-2"><i class="mdi mdi-arrow-right text-primary mr-1"></i>{{__('messages.option-3')}}</li>
                                    <li class="mt-2"><i class="mdi mdi-arrow-right text-primary mr-1"></i>{{__('messages.option-4')}}</li>
                                    <li class="mt-2"><i class="mdi mdi-arrow-right text-primary mr-1"></i>{{__('messages.option-5')}}</li>
                                    <li class="mt-2"><i class="mdi mdi-arrow-right text-primary mr-1"></i>{{__('messages.option-6')}}</li>
                                    <li class="mt-2"><i class="mdi mdi-arrow-right text-primary mr-1"></i>{{__('messages.option-7')}}</li>
                                    <li class="mt-2"><i class="mdi mdi-arrow-right text-primary mr-1"></i>{{__('messages.option-8')}}</li>
                                    <li class="mt-2"><i class="mdi mdi-arrow-right text-primary mr-1"></i>{{__('messages.option-9')}}</li>
                                    <li class="mt-2"><i class="mdi mdi-arrow-right text-primary mr-1"></i>{{__('messages.option-10')}}</li>
                                </ul></br>
                                <p class="text-muted">{{__('messages.last-para')}}</p>
                                <p class="text-muted">{{__('messages.last-para-01')}}</p>
                                <a href="{{ route('take-course') }}" class="btn btn-primary mt-3">Start Now</a>
                            </div>
                        </div>
                    </div><!--end col-->
                </div><!--end row-->
            </div><!--end container-->

            <div class="container mt-4">
                <div class="row justify-content-center">
                    <div class="col-lg-2 col-md-2 col-6 text-center pt-4">
                        <img src="images/client/amazon.svg" class="avatar avatar-ex-sm" alt="">
                    </div><!--end col-->

                    <div class="col-lg-2 col-md-2 col-6 text-center pt-4">
                        <img src="images/client/google.svg" class="avatar avatar-ex-sm" alt="">
                    </div><!--end col-->
                    
                    <div class="col-lg-2 col-md-2 col-6 text-center pt-4">
                        <img src="images/client/lenovo.svg" class="avatar avatar-ex-sm" alt="">
                    </div><!--end col-->
                    
                    <div class="col-lg-2 col-md-2 col-6 text-center pt-4">
                        <img src="images/client/paypal.svg" class="avatar avatar-ex-sm" alt="">
                    </div><!--end col-->
                    
                    <div class="col-lg-2 col-md-2 col-6 text-center pt-4">
                        <img src="images/client/shopify.svg" class="avatar avatar-ex-sm" alt="">
                    </div><!--end col-->
                    
                    <div class="col-lg-2 col-md-2 col-6 text-center pt-4">
                        <img src="images/client/spotify.svg" class="avatar avatar-ex-sm" alt="">
                    </div><!--end col-->
                </div><!--end row-->
            </div><!--end container-->
        </section><!--end section-->
        
        <section class="section bg-light">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-12">
                        <div class="section-title text-center mb-4 pb-2">
                            <h6 class="text-primary">Work Process</h6>
                            <h4 class="title mb-4">How do we works ?</h4>
                            <p class="text-muted para-desc mx-auto mb-0">Start working with <span class="text-primary font-weight-bold">future alert</span> that can provide everything you need to generate awareness, drive traffic, connect.</p>
                        </div>
                    </div><!--end col-->
                </div><!--end row-->

                <div class="row">
                    <div class="col-md-4 mt-4 pt-2">
                        <div class="card features work-process bg-transparent process-arrow border-0 text-center">
                            <div class="icons rounded h1 text-center text-primary px-3">
                                <i class="uil uil-presentation-edit"></i>
                            </div>

                            <div class="card-body">
                                <h4 class="title text-dark">Online-Coaching</h4>
                                <p class="text-muted mb-0">We conducting the several online classes for each courses for the availibily of the candidate</p>
                            </div>
                        </div>
                    </div><!--end col-->
                    
                    <div class="col-md-4 mt-md-5 pt-md-3 mt-4 pt-2">
                        <div class="card features work-process bg-transparent process-arrow border-0 text-center">
                            <div class="icons rounded h1 text-center text-primary px-3">
                                <i class="uil uil-airplay"></i>
                            </div>

                            <div class="card-body">
                                <h4 class="title text-dark">Strategy & Techniques</h4>
                                <p class="text-muted mb-0">We suggesting different technique and strategy and techniques for your career building courses.</p>
                            </div>
                        </div>
                    </div><!--end col-->
                    
                    <div class="col-md-4 mt-md-5 pt-md-5 mt-4 pt-2">
                        <div class="card features work-process bg-transparent d-none-arrow border-0 text-center">
                            <div class="icons rounded h1 text-center text-primary px-3">
                                <i class="uil uil-image-check"></i>
                            </div>

                            <div class="card-body">
                                <h4 class="title text-dark">Guiding</h4>
                                <p class="text-muted mb-0">We supporting and Guiding the candidate for the success.</p>
                            </div>
                        </div>
                    </div><!--end col-->
                </div><!--end row-->
            </div><!--end container-->

            <div class="container mt-100 mt-60">
                <div class="row align-items-end mb-4 pb-4">
                    <div class="col-md-8">
                        <div class="section-title text-center text-md-left">
                            <h6 class="text-primary">Services</h6>
                            <h4 class="title mb-4">What we do ?</h4>
                            <p class="text-muted mb-0 para-desc">Start working with <span class="text-primary font-weight-bold">future alert</span> that can provide everything you need to generate awareness, connect.</p>
                        </div>
                    </div><!--end col-->

                    <!-- <div class="col-md-4 mt-4 mt-sm-0">
                        <div class="text-center text-md-right">
                            <a href="javascript:void(0)" class="text-primary h6">See More <i data-feather="arrow-right" class="fea icon-sm"></i></a>
                        </div>
                    </div> -->
                    <!--end col-->
                </div><!--end row-->

                <div class="row">
                    <div class="col-md-4 mt-4 pt-2">
                        <ul class="nav nav-pills nav-justified flex-column bg-white rounded shadow p-3 mb-0 sticky-bar" id="pills-tab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link rounded active" id="webdeveloping" data-toggle="pill" href="#developing" role="tab" aria-controls="developing" aria-selected="false">
                                    <div class="text-center pt-1 pb-1">
                                        <h6 class="title font-weight-normal mb-0">Matching Scholarship</h6>
                                    </div>
                                </a><!--end nav link-->
                            </li><!--end nav item-->
                            
                            <li class="nav-item mt-2">
                                <a class="nav-link rounded" id="database" data-toggle="pill" href="#data-analise" role="tab" aria-controls="data-analise" aria-selected="false">
                                    <div class="text-center pt-1 pb-1">
                                        <h6 class="title font-weight-normal mb-0">Online Coaching</h6>
                                    </div>
                                </a><!--end nav link-->
                            </li><!--end nav item-->
                            
                            <li class="nav-item mt-2">
                                <a class="nav-link rounded" id="server" data-toggle="pill" href="#security" role="tab" aria-controls="security" aria-selected="false">
                                    <div class="text-center pt-1 pb-1">
                                        <h6 class="title font-weight-normal mb-0">Safe Guarding students's right</h6>
                                    </div>
                                </a><!--end nav link-->
                            </li><!--end nav item-->
                            
                            <li class="nav-item mt-2">
                                <a class="nav-link rounded" id="webdesigning" data-toggle="pill" href="#designing" role="tab" aria-controls="designing" aria-selected="false">
                                    <div class="text-center pt-1 pb-1">
                                        <h6 class="title font-weight-normal mb-0">Notification</h6>
                                    </div>
                                </a><!--end nav link-->
                            </li><!--end nav item-->
                        </ul><!--end nav pills-->
                    </div><!--end col-->

                    <div class="col-md-8 col-12 mt-4 pt-2">
                        <div class="tab-content" id="pills-tabContent">
                            <div class="tab-pane fade bg-white show active p-4 rounded shadow" id="developing" role="tabpanel" aria-labelledby="webdeveloping">
                                <img src="{{ asset('frontend/assets/images/work/about04.png') }}" class="img-fluid rounded shadow" alt="">
                                <div class="mt-4">
                                    <p class="text-muted">Intelligent Scholarship Matching & recommendation system.</p>
                                </div>
                            </div><!--end teb pane-->
                            
                            <div class="tab-pane fade bg-white p-4 rounded shadow" id="data-analise" role="tabpanel" aria-labelledby="database">
                                <img src="{{ asset('frontend/assets/images/work/about03.png') }}" class="img-fluid rounded shadow" alt="">
                                <div class="mt-4">
                                    <p class="text-muted">Dedicated and intensive online coaching for submitting edu-loan (
<a href="www.vidyalakshmi.co.in">www.vidyalakshmi.co.in</a> ) application , scholarship ( <a href="www.vidyasaarathi.co.in">www.vidyasaarathi.co.in</a> ) application through
online. Also provides intensive online coaching for PSC exams, K -TT exams etc.</p>
                                </div>
                            </div><!--end teb pane-->

                            <div class="tab-pane fade bg-white p-4 rounded shadow" id="security" role="tabpanel" aria-labelledby="server">
                                <img src="{{ asset('frontend/assets/images/work/about02.png') }}" class="img-fluid rounded shadow" alt="">
                                <div class="mt-4">
                                    <p class="text-muted">Drafting various complaints & processing applications unde RTI Act 2005,which will enable every students to safeguard their Rights.</p>
                            
                                </div>
                            </div><!--end teb pane-->
                            
                            <div class="tab-pane fade bg-white p-4 rounded shadow" id="designing" role="tabpanel" aria-labelledby="webdesigning">
                                <img src="{{ asset('frontend/assets/images/work/about01.png') }}" class="img-fluid rounded shadow" alt="">
                                <div class="mt-4">
                                    <p class="text-muted">Regular alerts on new / upcoming scholarships, various entrance exams etc.</p>
                            
                                </div>
                            </div><!--end teb pane-->
                        </div><!--end tab content-->
                    </div><!--end col-->
                </div><!--end row-->
            </div><!--end container-->
        </section><!--end section-->

        <!-- Start -->
        
        <!-- End -->

@endsection