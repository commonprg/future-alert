<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
                <!-- <x-jet-welcome /> -->
                <!-- Styles -->
                <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800,900&display=swap" rel="stylesheet">
                <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
                <link href="{{ asset('dashboard-theme/assets/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
                <link href="{{ asset('dashboard-theme/assets/plugins/font-awesome/css/all.min.css') }}" rel="stylesheet"> 

                <!-- Theme Styles -->
                <link href="{{ asset('dashboard-theme/assets/css/lime.min.css') }}" rel="stylesheet">
                <link href="{{ asset('dashboard-theme/assets/css/custom.css') }}" rel="stylesheet">

                <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
                <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
                <!--[if lt IE 9]>
                <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
                <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
                <![endif]-->
                <div class="lime-container">
                    <div class="lime-body">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="card bg-info text-white">
                                        <div class="card-body">
                                            <div class="dashboard-info row">
                                                <div class="info-text col-md-6">
                                                    <h5 class="card-title">Welcome back {{ Auth::user()->firstname}}</h5>
                                                    <p>Get alert for the notification alert for the class.</p>
                                                    <ul>
                                                        <li>We conducting class for the guidence of taking edu-loan.</li>
                                                        <li>Supporting for collecting data</li>
                                                        <li>Supporting for the paper work.</li>
                                                    </ul>
                                                </div>
                                                <div class="info-image col-md-6"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>      
                                <div class="col-md-4">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="">
                                                <div class="">
                                                    <h5 class="card-title">Daily Visitors</h5>
                                                    <canvas id="visitorsChart"></canvas>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>                      
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="card stat-card">
                                        <div class="card-body">
                                            <h5 class="card-title">Class attended</h5>
                                            <h2 class="float-right">0</h2>
                                            <p>From last week</p>
                                            <div class="progress" style="height: 10px;">
                                                <div class="progress-bar bg-warning" role="progressbar" style="width: 45%" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="card stat-card">
                                        <div class="card-body">
                                            <h5 class="card-title">Orders</h5>
                                            <h2 class="float-right">14.3K</h2>
                                            <p>Orders in waitlist</p>
                                            <div class="progress" style="height: 10px;">
                                                <div class="progress-bar bg-info" role="progressbar" style="width: 60%" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="card stat-card">
                                        <div class="card-body">
                                            <h5 class="card-title">Monthly Profit</h5>
                                            <h2 class="float-right">45.6$</h2>
                                            <p>For last 30 days</p>
                                            <div class="progress" style="height: 10px;">
                                                <div class="progress-bar bg-success" role="progressbar" style="width: 45%" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @if(Session::get('user_data'))
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="card">
                                        <div class="card-body">
                                            <h5 class="card-title">Offer Letters</h5>
                                            <div class="table-responsive">
                                                <table class="table">
                                                    <thead>
                                                        <tr>
                                                            <th scope="col">Offer Letter</th>
                                                            <th scope="col">Download</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    @foreach(Session::get('user_data') as $doc)
                                                        <tr>
                                                            <td>{{ $doc }}</td>
                                                            <td><a href="{{ route('docview','') }}/{{ $doc }}" target="_blank"><span class="badge badge-success">View</span></a></td>
                                                            @if($message = Session::get('error'))
                                                                <div class="alert alert-danger alert-dismissible fade in" role="alert">
                                                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                                        <span aria-hidden="true">×</span>
                                                                    </button>
                                                                    <strong>Error!</strong> {{ $message }}
                                                                </div>
                                                            @endif
                                                            {!! Session::forget('error') !!}
                                                            @if($message = Session::get('success'))
                                                                <div class="alert alert-info alert-dismissible fade in" role="alert">
                                                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                                        <span aria-hidden="true">×</span>
                                                                    </button>
                                                                    <strong>Success!</strong> {{ $message }}
                                                                </div>
                                                            @endif
                                                            {!! Session::forget('success') !!}
                                                            <td><form action="{{route('razorpaypayment')}}" method="POST" >                        
                                                                <script src="https://checkout.razorpay.com/v1/checkout.js"
                                                                        data-key="{{ env('RAZORPAY_KEYID') }}"
                                                                        data-amount="50000"
                                                                        data-buttontext="Pay"
                                                                        data-name="Future Alert"
                                                                        data-description="Let's Move Together"
                                                                        data-image="{{ asset('frontend/assets/images/falert.png') }}"
                                                                        data-prefill.name="name"
                                                                        data-prefill.email="email"
                                                                        data-theme.color="#2f55d4">
                                                                </script>
                                                                <input type="hidden" name="_token" value="{!!csrf_token()!!}">
                                                            </form></td>
                                                            <!-- <td><a href="{{route('razorpaypayment')}}" target=""><span class="badge badge-info">Pay</span></a></td> -->
                                                        </tr>
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                            </div>      
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endif
                        </div>
                    </div>
                </div>
                 <!-- Javascripts -->
                <script src="{{ asset('dashboard-theme/assets/plugins/jquery/jquery-3.1.0.min.js') }}"></script>
                <script src="{{ asset('dashboard-theme/assets/plugins/bootstrap/popper.min.js') }}"></script>
                <script src="{{ asset('dashboard-theme/assets/plugins/bootstrap/js/bootstrap.min.js') }}"></script>
                <script src="{{ asset('dashboard-theme/assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>
                <script src="{{ asset('dashboard-theme/assets/plugins/chartjs/chart.min.js') }}"></script>
                <script src="{{ asset('dashboard-theme/assets/plugins/apexcharts/dist/apexcharts.min.js') }}"></script>
                <script src="{{ asset('dashboard-theme/assets/js/lime.min.js') }}"></script>
                <script src="{{ asset('dashboard-theme/assets/js/pages/dashboard.js') }}"></script> 
            </div>
        </div>
    </div>
</x-app-layout>
