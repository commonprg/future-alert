@extends('admin.layouts.app')
@section('headerClass','')
@section('content')
<div class="container mt-5 mb-5">
    <h2 class="mb-4">Shortlisted Candidates</h2>
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN Portlet -->
            <div class="portlet">
                <div class="portlet-body">
                    <form id="frm-example" class="">
                    @csrf
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="portlet-title" for="inlineFormCustomSelectPref">Users</label><br>
                            <!-- BEGIN Custom Select -->
                            <select class="custom-select" name="user_id" id="users" style="color:#000">
                                <option value="" selected disabled>Select Candidates...</option>
                                @foreach($data as $user)
                                    <option value="{{ $user->id }}">{{ $user->name }}</option>
                                @endforeach
                            </select>
                            <!-- END Custom Select -->     
                        </div>  
                        <div class="form-group">
                            <label class="portlet-title" for="exampleFormControlTextarea1">Message</label>
                            <textarea class="form-control" id="rplyMessage" name="rplyMessage" rows="3"></textarea>
                        </div>
                    </div>
                    <div class="col-md-12 text-center">
                        <hr>
                        <button type="submit" id="submit" class="btn btn-primary my-1">Submit</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
<script type="text/javascript">
$(document).ready(function() {
    $("#submit").click(function (event) {
        // Prevent actual form submission
        event.preventDefault();
    
        // disabled the submit button
        $("#submit").prop("disabled", true);

        // Get form
        var form = $('#frm-example')[0];

        // FormData object 
        var data = new FormData(form);

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        });

        $.ajax({
            type: "POST",
            enctype: 'multipart/form-data',
            url: "{{ route('admin.replyEmail') }}",
            data: data,
            processData: false,
            contentType: false,
            cache: false,
            timeout: 800000,
            success: function (data) {
                $("#submit").prop("disabled", false);
                $('#frm-example')[0].reset();
            },
            error: function (e) {
                console.log("ERROR : ", e);
                $("#submit").prop("disabled", false);
            }
        });
    });   
});
</script>
@endpush
<!-- END Page Content -->
