@extends('admin.layouts.app')
@section('headerClass','')
@section('content')
<div class="container-fluid mt-5 mb-5">
    <h2 class="mb-4">User Details</h2>
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN Portlet -->
            <div class="portlet">
                <div class="portlet-body">
                    <table class="table table-bordered yajra-datatable">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Phone</th>
                                <th>Category</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script type="text/javascript">
  $(function () {
    
    var table = $('.yajra-datatable').DataTable({
        processing: true,
        serverSide: true,
        ajax: "{{ route('admin.getUsers') }}",
        columns: [
            {
                "data": 'DT_RowIndex',
                orderable: false, 
                searchable: false
            },
            {data: 'name', name: 'name'},
            {data: 'email', name: 'email'},
            {data: 'phone', name: 'phone'},
            {data: 'category', name: 'category'},
        ]
    });
    
  });
</script>
@endpush
<!-- END Page Content -->

