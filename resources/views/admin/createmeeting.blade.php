@extends('admin.layouts.app')
@section('headerClass','')
@section('content')
<div class="container-fluid mt-5 mb-5">
   <h2 class="mb-4">Create Meeting</h2>    
   <div class="row">
      <div class="col-md-12">
            <!-- BEGIN Portlet -->
         <div class="portlet">
            <div class="portlet-body">
               <form id="frm-example" method="post">
               @csrf
                  <!-- <div class="form-row"> -->
                     <div class="portlet-body row">
                        <div class="col form-group ">
                           <input type="text" class="form-control" id="Meeting_title" name="Meeting_title" placeholder="Meeting Title">
                        </div>
                        <div class="col form-group ">
                           <input type="text" class="form-control" id="Meeting_link" name="Meeting_link" placeholder="Meeting Link">
                        </div>
                        <div class='col form-group input-group date' id='datetimepicker'>
                           <input type='text' class="form-control" id="Meeting_date" name="Meeting_date" placeholder="Meeting Date and Time" />
                           <span class="input-group-addon">
                           <span class="glyphicon glyphicon-calendar"></span>
                           </span>
                        </div>
                     </div>
                  <!-- </div> -->
                  <hr>
                  <div class="portlet-body">
                     <div class="row input-daterange">
                        <div class="col-md-4 pb-2">
                           <input type="text" name="from_date" id="from_date" class="form-control" placeholder="From Date" readonly />
                        </div>
                        <div class="col-md-4 pb-2">
                           <input type="text" name="to_date" id="to_date" class="form-control" placeholder="To Date" readonly />
                        </div>
                        <div class="col-md-4 pb-2">
                           <button type="button" name="filter" id="filter" class="btn btn-info" style="width: 65px;">Filter</button>
                           <button type="button" name="refresh" id="refresh" class="btn btn-info" style="width: 65px;">Refresh</button>
                        </div>
                     </div>
                  </div>
                  <br>
                  <table id="example" class="display" cellspacing="0" width="100%">
                     <thead>
                        <tr>
                           <th></th>
                           <th>No</th>
                           <th>Name</th>
                           <th>Email</th>
                           <th>Phone</th>
                           <th>Category</th>
                        </tr>
                     </thead>
                  </table>
                  <hr>
                  <p><button name=submit id="submit" class="btn btn-info">Submit</button></p>
               </form>
            </div>
         </div>
      </div>
   </div>
</div>
@endsection

@push('scripts')
<script type="text/javascript" src="//gyrocode.github.io/jquery-datatables-checkboxes/1.2.12/js/dataTables.checkboxes.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
   $(function() {
      $("#datetimepicker").datetimepicker({
         startDate: new Date(),
         todayHighlight: 'TRUE',
            autoClose: true,
      });
   });
   $('.input-daterange').datepicker({
      todayBtn:'linked',
      format:'yyyy-mm-dd',
      autoclose:true
   });
   var table = $('#example').DataTable({
      processing: true,
      serverSide: true,
      ajax: {
         url: "{{ route('admin.Meeting') }}",
         data: function (d) {
               d.from_date = $('#from_date').val();
               d.to_date = $('#to_date').val();   
         },
      },
      'columnDefs': [
         {
            "data": "id",
            'targets': 0,
            'checkboxes': {
               'selectRow': true
            }
         }
      ],
      'select': {
         'style': 'multi'
      },
      'order': [[1, 'asc']],
      "columns": [
         { "data": 'checkbox', name: 'checkbox', class: 'chkbox', orderable: false, searchable: false,},
         { "data": 'DT_RowIndex', orderable: true, searchable: false,},
         { "data": "name" },
         { "data": "email" },
         { "data": "phone" },
         { "data": "category" },
      ]
   });
   $("#submit").click(function (event) {
      // Prevent actual form submission
      event.preventDefault();

      // Get form
      var form = $('#frm-example')[0];

      // disabled the submit button
      $("#submit").prop("disabled", true);

      var rows = table.rows({ selected: true }).data();

      // Iterate over all selected checkboxes
      $.each(rows, function(index, element){
         // Create a hidden element 
         $('<input>').attr({
            type: 'hidden',
            name: 'id[]',
            value: element.id,
         }).appendTo('form');
      });

      // FormData object 
      var data = new FormData(form);

      $.ajaxSetup({
         headers: {
            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
         }
      });

      $.ajax({
         type: "POST",
         enctype: 'multipart/form-data',
         url: "{{ route('admin.crtMeeting') }}",
         data: data,
         processData: false,
         contentType: false,
         cache: false,
         timeout: 800000,
         success: function (data) {
            $("#submit").prop("disabled", false);
            $('#frm-example')[0].reset();
            $('#example').dataTable().fnDestroy();
            $('#example').dataTable();
         },
         error: function (e) {
            console.log("ERROR : ", e);
            $("#submit").prop("disabled", false);
         }
      });
   });
   $('#filter').click(function(){
      var from_date = $('#from_date').val();
      var to_date = $('#to_date').val();
      if(from_date != '' &&  to_date != '') { 
         table.draw();
      } else {
         alert('Select Date!');
      }
   });
   $('#refresh').click(function(){
      $('#from_date').val('');
      $('#to_date').val('');
      table.draw();
   });
});
</script>
@endpush
<!-- END Page Content -->

