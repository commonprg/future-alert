@extends('admin.layouts.app')
@section('headerClass','')
@section('content')
<div class="container-fluid mt-5 mb-5">
<h2 class="mb-4">Edit Hospital Details</h2>  
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN Portlet -->
            <div class="portlet">
                <div class="portlet-body">
                    <div class="rounded">
                        <input type="hidden"  id="hospital_id" value="{{$id}}" >
                        <form class="mt-4" id="form" method="POST" action="{{ route('admin.update',['id' => $id]) }}" >
                            @csrf
                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group position-relative">
                                        <label class="portlet-title">Name <span class="text-danger">*</span></label>
                                        <input name="name" id="name" type="text" class="form-control" placeholder="Name :" value ="{{ old('name') ? old('name') : $data->name }}">
                                        @error('name')
                                            <span class="invalid-feedback d-block" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div><!--end col-->
                                <div class="col-6">
                                    <div class="form-group position-relative">
                                        <label class="portlet-title">Your Email <span class="text-danger">*</span></label>
                                        <input name="email" id="email" type="email" class="form-control" placeholder="Your email :" value ="{{ old('email') ? old('email') : $data->email }}">
                                        @error('email')
                                            <span class="invalid-feedback d-block" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div> 
                                </div><!--end col-->
                                <div class="col-6">
                                    <div class="form-group position-relative">
                                        <label class="portlet-title">Phone <span class="text-danger">*</span></label>
                                        <input type="text" name="phone" id="phone" class="form-control" placeholder="Phone number :" value ="{{ old('phone') ? old('phone') : $data->phone}}">
                                        @error('phone')
                                            <span class="invalid-feedback d-block" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div><!--end col-->
                                <div class="col-md-6">
                                    <div class="form-group position-relative">
                                        <label class="portlet-title">State <span class="text-danger">*</span></label>
                                        <input type="text" name="state" id="state" class="form-control" placeholder="State Name :" value ="{{ old('state') ? old('state') :$data->state}}">
                                        @error('state')
                                            <span class="invalid-feedback d-block" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div><!--end col-->
                                <div class="col-md-6">
                                    <div class="form-group position-relative">
                                        <label class="portlet-title">Address <span class="text-danger">*</span></label>
                                        <textarea name="Address" id="Address" class="form-control" placeholder="Address :">{{ old('Address') ? old('Address') : $data->address}}</textarea>
                                        @error('Address')
                                            <span class="invalid-feedback d-block" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div><!--end col--> 
                                <div class="col-md-6">
                                    <div class="form-group position-relative">
                                        <label class="portlet-title">Postal Code <span class="text-danger">*</span></label>
                                        <input type="text" name="postcode" id="postcode" class="form-control" placeholder="Zip :" value ="{{ old('postcode') ? old('postcode') : $data->postalcode}}">
                                        @error('postcode')
                                            <span class="invalid-feedback d-block" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div><!--end col--> 
                                <div class="col-md-12 text-center">
                                <hr>
                                    <button class="btn btn-info">Update</button>
                                </div>
                            </div><!--end row-->
                        </form><!--end form-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div><!--end container-->
@endsection