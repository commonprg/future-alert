@extends('admin.layouts.app')
@section('headerClass','')
@section('content')
<div class="container-fluid mt-5 mb-5">
<h2 class="mb-4">Edit Meeting Details</h2>  
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN Portlet -->
            <div class="portlet">
                <div class="portlet-body">
                    <input type="hidden"  id="meeting_id" value="{{$id}}" >
                    <form class="mt-4" id="form" method="POST" action="{{ route('admin.meetingupdate',['id' => $id]) }}" >
                        @csrf
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group position-relative">
                                    <label>Meeting Title <span class="text-danger">*</span></label>
                                    <input name="title" id="title" type="text" class="form-control" placeholder="Meeting Title:" value ="{{old('title') ? old('title') :$data->title}}">
                                    @error('title')
                                        <span class="invalid-feedback d-block" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div><!--end col-->
                            <div class="col-6">
                                <div class="form-group position-relative">
                                    <label>Meeting Link <span class="text-danger">*</span></label>
                                    <input name="link" id="link" type="link" class="form-control" placeholder="Meeting link :" value ="{{old('link') ? old('link') :$data->link}}" readonly>
                                    @error('link')
                                        <span class="invalid-feedback d-block" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div> 
                            </div><!--end col-->
                            <div class="col-6">
                                <div class="form-group position-relative">
                                    <label>Meeting Date and Time <span class="text-danger">*</span></label>
                                    <input type="text" name="date_time" id="date_time" class="form-control" placeholder="Meeting date :" value ="{{old('date_time') ? old('date_time') :$data->date_time}}" readonly>
                                    @error('date_time')
                                        <span class="invalid-feedback d-block" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div><!--end col-->
                            <div class="col-md-6">
                                <div class="form-group position-relative">
                                    <label>Meeting Status <span class="text-danger">*</span></label>
                                    <input type="text" name="status" id="status" class="form-control" placeholder="Meeting Status :" value ="{{ old('status') ? old('status') : $data->status}}">
                                    @error('status')
                                        <span class="invalid-feedback d-block" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div><!--end col-->
                            <div class="col-md-12 text-center">
                                <hr>
                                <button class="btn btn-info">Update</button>
                            </div>
                        </div><!--end row-->
                    </form><!--end form-->
                </div>          
            </div>        
        </div>    
    </div>
</div><!--end container-->
@endsection