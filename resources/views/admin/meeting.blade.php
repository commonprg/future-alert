@extends('admin.layouts.app')
@section('headerClass','')
@section('content')
<div class="container-fluid mt-5 mb-5">
    <h2 class="mb-4">Meeting Details</h2>
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN Portlet -->
            <div class="portlet">
                <div class="portlet-body">
                    <table class="table table-bordered yajra-datatable">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Title</th>
                                <th>Link</th>
                                <th>Date</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script type="text/javascript">
  $(function () {
    var table = $('.yajra-datatable').DataTable({
        processing: true,
        serverSide: true,
        ajax: "{{ route('admin.getMeetings') }}",
        columns: [
            {
                "data": 'DT_RowIndex',
                orderable: false, 
                searchable: false
            },
            {data: 'title', name: 'title'},
            {data: 'link', name: 'link'},
            {data: 'date_time', name: 'date_time'},
            {data: 'status', name: 'status'},
        ],
        "columnDefs":[
            {"targets":5, "data":"id", "render": function(data,type,full,meta) { 
                var viewurl = "{{ route('admin.viewParticipants','') }}";
                var editurl = "{{ route('admin.editMeeting','') }}";
                return '<a href="'+ viewurl +'/'+ data +'" ><button class="btn btn-icon" style="width: auto;"><i class="fa fa-eye"></i> </button><a href="'+ editurl +'/'+ data +'" target="_blank"><button class="btn btn-icon" style="width: auto;"><i class="fa fa-edit"></i> </button>'
                }
            }
        ]
    });    
  });
</script>
@endpush
<!-- END Page Content -->

