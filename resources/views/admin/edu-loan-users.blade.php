@extends('admin.layouts.app')
@section('headerClass','')
@section('content')
<div class="container-fluid mt-5 mb-5">
<h2 class="mb-4">Edu-Loan User Details</h2>
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN Portlet -->
            <div class="portlet">
                <div class="portlet-body">
                    <table class="table table-bordered datatable">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Phone</th>
                                <!-- <th>State</th> -->
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script type="text/javascript">
    var table = $('.datatable').DataTable({
        processing: true,
        serverSide: true,
        ajax: "{{ route('admin.getEduloanUsers') }}",
        columns: [
            {
                "data": 'DT_RowIndex',
                orderable: false, 
                searchable: false
            },
            {data: 'name', name: 'name'},
            {data: 'email', name: 'email'},
            {data: 'phone', name: 'phone'},
            // {data: 'state', name: 'state'},
        ],

        "columnDefs":[
            {
                "targets":4, "data": "id", "render": function (data, type, full) {
                    var viewurl = "{{ route('admin.profileView','') }}";
                    return '<a href="'+ viewurl +'/'+ data +'" ><button class="btn btn-ico" style="width: auto;"><i class="fa fa-eye"></i> </button>'
                },
            },
        ]
    });
</script>
@endpush
<!-- END Page Content -->

