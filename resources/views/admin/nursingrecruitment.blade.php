@extends('admin.layouts.app')
@section('headerClass','')
@section('content')
<div class="container mt-5 mb-5">
<h2 class="mb-4">Nursing Recruitment Candidate Details</h2>
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN Portlet -->
            <div class="portlet">
                <div class="portlet-body">
                    <table class="table table-bordered datatable">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Image</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Phone</th>
                                <!-- <th>Register No</th>
                                <th>Year of Experience</th>
                                <th>Experience Letter</th> -->
                                <th>Download</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script type="text/javascript">
//   $(function () {
    var table = $('.datatable').DataTable({
        processing: true,
        serverSide: true,
        ajax: "{{ route('admin.getCandidates') }}",
        columns: [
            {
                "data": 'DT_RowIndex',
                orderable: false, 
                searchable: false
            },
            {
                "data": "image",
                "render": function (data) {
                    var flagsUrl = '{{Storage::disk("profile_photos")->url("public/profile_photos/","")}}';
                    return "<img src='"+flagsUrl+data+"' width='80' height='60'>";
                },
            },
            {data: 'name', name: 'name'},
            {data: 'email', name: 'email'},
            {data: 'phone', name: 'phone'},
            // {data: 'regNo', name: 'regNo'},
            // {data: 'yearofExp', name: 'yearofExp'},
            // {data: 'experienceLetter', name: 'experienceLetter'},
        ],   
        "columnDefs":[
            {
                "targets":5, "data": "id", "render": function (data, type, full) {
                    var viewurl = "{{ route('admin.profileView','') }}";
                    var downloadurl = "{{ route('admin.download','') }}";
                    return '<a href="'+ viewurl +'/'+ data +'" ><button class="btn btn-icon" style="width: auto;"><i class="fa fa-eye"></i> </button><a href="'+ downloadurl +'/'+ full['experienceLetter'] +'" ><button class="btn btn-icon" style="width: auto;"><i class="fa fa-download"></i> </button>'
                },
            },
        ]
    });
</script>
@endpush
<!-- END Page Content -->

