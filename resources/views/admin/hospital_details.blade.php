@extends('admin.layouts.app')
@section('headerClass','')
@section('content')
<div class="container-fluid mt-5 mb-5">
<h2 class="mb-4">Hospital Details</h2>
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN Portlet -->
            <div class="portlet">
                <div class="portlet-body">
                    <input type="hidden"  id="hospital_id" value="{{$id}}" >
                    <div class="row">
                        <div class="col-sm-3">
                            <h5 class="portlet-title">Name</h5>
                        </div>
                        <div class="col-sm-9 text-secondary">
                            <h5 class="portlet-title">{{$data->name}}</h5>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-sm-3">
                            <h5 class="portlet-title">Email</h5>
                        </div>
                        <div class="col-sm-9 text-secondary">
                            <h5 class="portlet-title">{{$data->email}}</h5>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-sm-3">
                            <h5 class="portlet-title">Phone</h5>
                        </div>
                        <div class="col-sm-9 text-secondary">
                            <h5 class="portlet-title"> {{$data->phone}}</h5>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-sm-3">
                            <h5 class="portlet-title">Postal Code</h5>
                        </div>
                        <div class="col-sm-9 text-secondary">
                            <h5 class="portlet-title">{{$data->postalcode}}</h5>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-sm-3">
                            <h5 class="portlet-title">Address</h5>
                        </div>
                        <div class="col-sm-9 ">
                            <h5 class="portlet-title">{{$data->address}}</h5>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-sm-3">
                            <h5 class="portlet-title">State</h5>
                        </div>
                        <div class="col-sm-9 ">
                            <h5 class="portlet-title">{{$data->state}}</h5>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-sm-12">
                            <a class="btn btn-info " target="__blank" href="{{ route('admin.edit',[$id]) }}">Edit</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
<!-- END Page Content -->

