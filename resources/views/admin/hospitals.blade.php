@extends('admin.layouts.app')
@section('headerClass','')
@section('content')
<div class="container-fluid mt-5 mb-5">
    <h2 class="mb-4">Hospital List</h2>
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN Portlet -->
            <div class="portlet">
                <div class="portlet-body">
                    <div class="col-md-12 text-right">
                        <a href="{{ URL::route('admin.addHospital') }}" class="btn btn-info"> Add Hospital </a>
                    </div>
                    <br>
                    <table class="table table-bordered yajra-datatable">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Phone</th>
                                <th>State</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script type="text/javascript">
  $(document).ready(function() {
    var table = $('.yajra-datatable').DataTable({
        responsive: true,
        processing: true,
        serverSide: true,
        ajax: "{{ route('admin.getHospitallist') }}",
        columns: [
            {
                "data": 'DT_RowIndex',
                orderable: false, 
                searchable: false
            },
            {data: 'name', name: 'name'},
            {data: 'email', name: 'email'},
            {data: 'phone', name: 'phone'},
            {data: 'state', name: 'state'},
            {
                "targets":5, "data": "id", "render": function (data) {
                    var viewurl = "{{ route('admin.view','') }}";
                    var editurl = "{{ route('admin.edit','') }}";
                    var deleteurl = "{{ route('admin.delete','') }}";
                    return '<a href="'+ viewurl +'/'+ data +'" ><button class="btn btn-icon" style="width: auto;"><i class="fa fa-eye"></i> </button><a href="'+ editurl +'/'+ data +'" ><button class="btn btn-icon" style="width: auto;"><i class="fa fa-edit"></i> </button><a href="'+ deleteurl +'/'+ data +'" ><button class="btn btn-icon" style="width: auto;"><i class="fa fa-trash"></i> </button>'
                }
            },
        ],
    });
  });
</script>
@endpush
<!-- END Page Content -->

