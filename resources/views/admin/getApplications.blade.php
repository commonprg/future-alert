@extends('admin.layouts.app')
@section('headerClass','')
@section('content')
<div class="container-fluid mt-5 mb-5">
<h2 class="mb-4">Candidate Application</h2>
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN Portlet -->
            <div class="portlet">
                <div class="portlet-body">
                    <form id="frm-example" >
                    @csrf
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label class="my-1 mr-2 portlet-title" for="inlineFormCustomSelectPref">Users</label><br>
                                <!-- BEGIN Custom Select -->
                                <select class="form-control js-example-tokenizer my-2 mr-sm-2" name="user_id[]" id="users" multiple="multiple">
                                    @foreach($data as $user)
                                        <option value="{{ $user->id }}">{{ $user->name }}</option>
                                    @endforeach
                                </select>
                                <br>
                                <label class="my-1 mr-2 portlet-title" for="inlineFormCustomSelectPref">Hospitals</label>
                                <!-- BEGIN Custom Select -->
                                <select class="form-control js-example-tokenizer my-2 mr-sm-2" name="hospital_id[]" id="hospitals" multiple="multiple">
                                    @foreach($list as $hospital)
                                        <option value="{{ $hospital->id }}">{{ $hospital->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <!-- END Custom Select -->
                        <div class="col-md-12 text-center">
                            <hr>
                            <button type="submit" id="submit" class="btn btn-primary my-1">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
<script type="text/javascript">
$(document).ready(function() {
    var hospitalID;
    var userID;
    $(".js-example-tokenizer").select2({
        tags: true,
        tokenSeparators: [',', ' ']
    });
    $('#hospitals').on('change',function(){
        hospitalID = $(this).val(); 
    });
    $('#users').on('change',function(){
        userID = $(this).val(); 
    }); 
    $("#submit").click(function (event) {
        // Prevent actual form submission
        event.preventDefault();
    
        // disabled the submit button
        $("#submit").prop("disabled", true);

        // Get form
        var form = $('#frm-example')[0];

        // FormData object 
        var data = new FormData(form);

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        });

        $.ajax({
            type: "POST",
            enctype: 'multipart/form-data',
            url: "{{ route('admin.selectedList') }}",
            data: data,
            processData: false,
            contentType: false,
            cache: false,
            timeout: 800000,
            success: function (data) {
                // $("#hospitals").reset();
                $('#hospitals').prop('selectedIndex',0);
                $("#submit").prop("disabled", false);
            },
            error: function (e) {
                console.log("ERROR : ", e);
                $("#submit").prop("disabled", false);
            }
        });
    });   
});
</script>
@endpush
<!-- END Page Content -->
