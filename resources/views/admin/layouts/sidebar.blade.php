      <!-- BEGIN Aside -->
      <div class="aside">
        <div class="aside-header">
          <h3 class="aside-title">Panely</h3>
          <div class="aside-addon">
            <button
              class="btn btn-label-primary btn-icon btn-lg"
              data-toggle="aside"
            >
              <i class="fa fa-times aside-icon-minimize"></i>
              <i class="fa fa-thumbtack aside-icon-maximize"></i>
            </button>
          </div>
        </div>
        <div class="aside-body" data-simplebar="data-simplebar">
          <!-- BEGIN Menu -->
          <div class="menu">
            <div class="menu-item">
              <a
                href="{{ route('admin.dashboard') }}"
                data-menu-path=""
                class="menu-item-link"
              >
                <div class="menu-item-icon">
                  <i class="fa fa-desktop"></i>
                </div>
                <span class="menu-item-text">Dashboard</span>
                <div class="menu-item-addon">
                  <span class="badge badge-success">New</span>
                </div>
              </a>
            </div>
            <!-- BEGIN Menu Section -->
            <div class="menu-section">
              <div class="menu-section-icon">
                <i class="fa fa-ellipsis-h"></i>
              </div>
              <h2 class="menu-section-text">Elements</h2>
            </div>
            <!-- END Menu Section -->
            <div class="menu-item">
              <button class="menu-item-link menu-item-toggle">
                <div class="menu-item-icon">
                  <i class="fa fa-palette"></i>
                </div>
                <span class="menu-item-text">Base</span>
                <div class="menu-item-addon">
                  <i class="menu-item-caret caret"></i>
                </div>
              </button>
              <!-- BEGIN Menu Submenu -->
              <div class="menu-submenu">
                <div class="menu-item">
                  <a
                    href="{{ asset('admin-theme/ltr/elements/base/accordion.html') }}"
                    data-menu-path="{{ asset('admin-theme/ltr/elements/base/accordion.html') }}"
                    class="menu-item-link"
                  >
                    <i class="menu-item-bullet"></i>
                    <span class="menu-item-text">Accordion</span>
                  </a>
                </div>
                <div class="menu-item">
                  <a
                    href="{{ asset('admin-theme/ltr/elements/base/alert.html') }}"
                    data-menu-path="{{ asset('admin-theme/ltr/elements/base/alert.html') }}"
                    class="menu-item-link"
                  >
                    <i class="menu-item-bullet"></i>
                    <span class="menu-item-text">Alert</span>
                  </a>
                </div>
                <div class="menu-item">
                  <a
                    href="{{ asset('admin-theme/ltr/elements/base/badge.html') }}"
                    data-menu-path="{{ asset('admin-theme/ltr/elements/base/badge.html') }}"
                    class="menu-item-link"
                  >
                    <i class="menu-item-bullet"></i>
                    <span class="menu-item-text">Badge</span>
                  </a>
                </div>
                <div class="menu-item">
                  <a
                    href="{{ asset('admin-theme/ltr/elements/base/button.html') }}"
                    data-menu-path="{{ asset('admin-theme/ltr/elements/base/button.html') }}"
                    class="menu-item-link"
                  >
                    <i class="menu-item-bullet"></i>
                    <span class="menu-item-text">Button</span>
                  </a>
                </div>
                <div class="menu-item">
                  <a
                    href="{{ asset('admin-theme/ltr/elements/base/button-group.html') }}"
                    data-menu-path="{{ asset('admin-theme/ltr/elements/base/button-group.html') }}"
                    class="menu-item-link"
                  >
                    <i class="menu-item-bullet"></i>
                    <span class="menu-item-text">Button group</span>
                  </a>
                </div>
                <div class="menu-item">
                  <a
                    href="{{ asset('admin-theme/ltr/elements/base/card.html') }}"
                    data-menu-path="{{ asset('admin-theme/ltr/elements/base/card.html') }}"
                    class="menu-item-link"
                  >
                    <i class="menu-item-bullet"></i>
                    <span class="menu-item-text">Card</span>
                  </a>
                </div>
                <div class="menu-item">
                  <a
                    href="{{ asset('admin-theme/ltr/elements/base/color.html') }}"
                    data-menu-path="{{ asset('admin-theme/ltr/elements/base/color.html') }}"
                    class="menu-item-link"
                  >
                    <i class="menu-item-bullet"></i>
                    <span class="menu-item-text">Color</span>
                  </a>
                </div>
                <div class="menu-item">
                  <a
                    href="{{ asset('admin-theme/ltr/elements/base/dropdown.html') }}"
                    data-menu-path="{{ asset('admin-theme/ltr/elements/base/dropdown.html') }}"
                    class="menu-item-link"
                  >
                    <i class="menu-item-bullet"></i>
                    <span class="menu-item-text">Dropdown</span>
                  </a>
                </div>
                <div class="menu-item">
                  <a
                    href="{{ asset('admin-theme/ltr/elements/base/grid-nav.html') }}"
                    data-menu-path="{{ asset('admin-theme/ltr/elements/base/grid-nav.html') }}"
                    class="menu-item-link"
                  >
                    <i class="menu-item-bullet"></i>
                    <span class="menu-item-text">Grid navigation</span>
                  </a>
                </div>
                <div class="menu-item">
                  <a
                    href="{{ asset('admin-theme/ltr/elements/base/list-group.html') }}"
                    data-menu-path="{{ asset('admin-theme/ltr/elements/base/list-group.html') }}"
                    class="menu-item-link"
                  >
                    <i class="menu-item-bullet"></i>
                    <span class="menu-item-text">List group</span>
                  </a>
                </div>
                <div class="menu-item">
                  <a
                    href="{{ asset('admin-theme/ltr/elements/base/marker.html') }}"
                    data-menu-path="{{ asset('admin-theme/ltr/elements/base/marker.html') }}"
                    class="menu-item-link"
                  >
                    <i class="menu-item-bullet"></i>
                    <span class="menu-item-text">Marker</span>
                  </a>
                </div>
                <div class="menu-item">
                  <a
                    href="{{ asset('admin-theme/ltr/elements/base/modal.html') }}"
                    data-menu-path="{{ asset('admin-theme/ltr/elements/base/modal.html') }}"
                    class="menu-item-link"
                  >
                    <i class="menu-item-bullet"></i>
                    <span class="menu-item-text">Modal</span>
                  </a>
                </div>
                <div class="menu-item">
                  <a
                    href="{{ asset('admin-theme/ltr/elements/base/nav.html') }}"
                    data-menu-path="{{ asset('admin-theme/ltr/elements/base/nav.html') }}"
                    class="menu-item-link"
                  >
                    <i class="menu-item-bullet"></i>
                    <span class="menu-item-text">Navigation</span>
                  </a>
                </div>
                <div class="menu-item">
                  <a
                    href="{{ asset('admin-theme/ltr/elements/base/pagination.html') }}"
                    data-menu-path="{{ asset('admin-theme/ltr/elements/base/pagination.html') }}"
                    class="menu-item-link"
                  >
                    <i class="menu-item-bullet"></i>
                    <span class="menu-item-text">Pagination</span>
                  </a>
                </div>
                <div class="menu-item">
                  <a
                    href="{{ asset('admin-theme/ltr/elements/base/popover.html') }}"
                    data-menu-path="{{ asset('admin-theme/ltr/elements/base/popover.html') }}"
                    class="menu-item-link"
                  >
                    <i class="menu-item-bullet"></i>
                    <span class="menu-item-text">Popover</span>
                  </a>
                </div>
                <div class="menu-item">
                  <a
                    href="{{ asset('admin-theme/ltr/elements/base/progress.html') }}"
                    data-menu-path="{{ asset('admin-theme/ltr/elements/base/progress.html') }}"
                    class="menu-item-link"
                  >
                    <i class="menu-item-bullet"></i>
                    <span class="menu-item-text">Progress</span>
                  </a>
                </div>
                <div class="menu-item">
                  <a
                    href="{{ asset('admin-theme/ltr/elements/base/spinner.html') }}"
                    data-menu-path="{{ asset('admin-theme/ltr/elements/base/spinner.html') }}"
                    class="menu-item-link"
                  >
                    <i class="menu-item-bullet"></i>
                    <span class="menu-item-text">Spinner</span>
                  </a>
                </div>
                <div class="menu-item">
                  <a
                    href="{{ asset('admin-theme/ltr/elements/base/tab.html') }}"
                    data-menu-path="{{ asset('admin-theme/ltr/elements/base/tab.html') }}"
                    class="menu-item-link"
                  >
                    <i class="menu-item-bullet"></i>
                    <span class="menu-item-text">Tab</span>
                  </a>
                </div>
                <div class="menu-item">
                  <a
                    href="{{ asset('admin-theme/ltr/elements/base/table.html') }}"
                    data-menu-path="{{ asset('admin-theme/ltr/elements/base/table.html') }}"
                    class="menu-item-link"
                  >
                    <i class="menu-item-bullet"></i>
                    <span class="menu-item-text">Table</span>
                  </a>
                </div>
                <div class="menu-item">
                  <a
                    href="{{ asset('admin-theme/ltr/elements/base/tooltip.html') }}"
                    data-menu-path="{{ asset('admin-theme/ltr/elements/base/tooltip.html') }}"
                    class="menu-item-link"
                  >
                    <i class="menu-item-bullet"></i>
                    <span class="menu-item-text">Tooltip</span>
                  </a>
                </div>
                <div class="menu-item">
                  <a
                    href="{{ asset('admin-theme/ltr/elements/base/type.html') }}"
                    data-menu-path="{{ asset('admin-theme/ltr/elements/base/type.html') }}"
                    class="menu-item-link"
                  >
                    <i class="menu-item-bullet"></i>
                    <span class="menu-item-text">Typography</span>
                  </a>
                </div>
              </div>
              <!-- END Menu Submenu -->
            </div>
            <div class="menu-item">
              <a
                href="{{ route('admin.getUsers') }}"
                data-menu-path=""
                class="menu-item-link"
              >
                <div class="menu-item-icon">
                  <i class="fa fa-users"></i>
                </div>
                <span class="menu-item-text">Users</span>
              </a>
            </div>
            <div class="menu-item">
              <a
                href="{{ route('admin.Meeting') }}"
                data-menu-path=""
                class="menu-item-link"
              >
                <div class="menu-item-icon">
                  <i class="fa fa-handshake"></i>
                </div>
                <span class="menu-item-text">Create Meeting</span>
              </a>
            </div>
            <div class="menu-item">
              <a
                href="{{ route('admin.getMeetings') }}"
                data-menu-path=""
                class="menu-item-link"
              >
                <div class="menu-item-icon">
                  <i class="fa fa-stream"></i>
                </div>
                <span class="menu-item-text">Meetings</span>
              </a>
            </div>
            <div class="menu-item">
              <a
                href="{{ route('admin.getCandidates') }}"
                data-menu-path=""
                class="menu-item-link"
              >
                <div class="menu-item-icon">
                  <i class="fa fa-user"></i>
                </div>
                <span class="menu-item-text">Nursing Recruitment Users</span>
              </a>
            </div>
            <div class="menu-item">
              <a
                href="{{ route('admin.getEduloanUsers') }}"
                data-menu-path=""
                class="menu-item-link"
              >
                <div class="menu-item-icon">
                  <i class="fa fa-user"></i>
                </div>
                <span class="menu-item-text">Edu-Loan Users</span>
              </a>
            </div>
            <div class="menu-item">
              <a
                href="{{ route('admin.getHospitals') }}"
                data-menu-path=""
                class="menu-item-link"
              >
                <div class="menu-item-icon">
                  <i class="fa fa-hospital"></i>
                </div>
                <span class="menu-item-text">Hospitals</span>
              </a>
            </div>
            <div class="menu-item">
              <a
                href="{{ route('admin.getApplications') }}"
                data-menu-path=""
                class="menu-item-link"
              >
                <div class="menu-item-icon">
                  <i class="fa fa-envelope"></i>
                </div>
                <span class="menu-item-text">Candidate Applications</span>
              </a>
            </div>
            <div class="menu-item">
              <a
                href="{{ route('admin.getShortlistedCandidate') }}"
                data-menu-path=""
                class="menu-item-link"
              >
                <div class="menu-item-icon">
                  <i class="fa fa-tasks"></i>
                </div>
                <span class="menu-item-text">Shortlisted Candidates</span>
              </a>
            </div>
          </div>
          <!-- END Menu -->
        </div>
      </div>
      <!-- END Aside -->