@extends('admin.layouts.app')
@section('headerClass','')
@section('content')
<div class="container-fluid mt-5 mb-5">
    <h2 class="mb-4">Candidate Details</h2>
    @if($data->category=="nursing recruitment")
    <div class="row">
        <div class="col-md-6">
            <!-- BEGIN Portlet -->
            <div class="portlet">
                <div class="portlet-header portlet-header-bordered">
                    <h3 class="portlet-title">Hospitals</h3>
                </div>
                <div class="portlet-body">
                    <!-- BEGIN Form Group -->
                    <div class="form-group mb-0">
                        <form id="frm-example">
                        @csrf
                            <input type="hidden" name="user_id" id="user_id" value="{{$id}}" >
                            <!-- BEGIN Custom Select -->
                            <select class="form-control js-example-tokenizer my-2 mr-sm-2" name="hospital_id[]" id="hospital_id" multiple="multiple" style="width:200px!important">
                                @foreach($list as $hospital)
                                    <option value="{{ $hospital->id }}">{{ $hospital->name }}</option>
                                @endforeach
                            </select>
                            <!-- END Custom Select -->
                            <hr>
                            <button type="submit" id="submit" class="btn btn-primary my-1">Submit</button>
                        </form>
                    </div>
                    <!-- END Form Group -->
                </div>
            </div>
            <!-- END Portlet -->
        </div>
        <div class="col-md-6">
            <!-- BEGIN Portlet -->
            <div class="portlet">
                <div class="portlet-header portlet-header-bordered">
                    <h3 class="portlet-title">Upload Document</h3>
                </div>
                <div class="portlet-body">
                    <!-- BEGIN Form Group -->
                    <div class="form-group mb-0">
                        <form id="frm-upload">
                            @csrf
                            <input type="hidden" name="candidate_id" id="candidate_id" value="{{$id}}" >
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" name="customFile[]" id="customFile" multiple required>
                                <label class="custom-file-label" for="customFile"></label>
                            </div>
                          
                            @error('customFile[]')
                                <span class="invalid-feedback d-block" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                            <hr>
                            <button type="submit" id="upload" class="btn btn-primary my-1">Upload</button>
                        </form>
                    </div>
                    <!-- END Form Group -->
                </div>
            </div>
            <!-- END Portlet -->
        </div>
    </div>
    @endif
    <div class="portlet">
            <div class="portlet-body">
                <div class="row">
                    <div class="col-sm-3">
                        <h3 class="portlet-title">Name</h3>
                    </div>
                    <div class="col-sm-9">
                        <h3 class="portlet-title">{{$data->name}}</h5>
                    </div>
                </div>
                <hr>
                @if($data->regNo)
                    <div class="row">
                        <div class="col-sm-3">
                            <h3 class="portlet-title">register Number</h5>
                        </div>
                        <div class="col-sm-9">
                            <h3 class="portlet-title">{{$data->regNo}}</h5>
                        </div>
                    </div>
                    <hr>
                @endif
                <div class="row">
                    <div class="col-sm-3">
                    <h3 class="portlet-title">Email</h5>
                    </div>
                    <div class="col-sm-9">
                    <h3 class="portlet-title">{{$data->email}}</h5>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-sm-3">
                    <h3 class="portlet-title">Phone</h5>
                    </div>
                    <div class="col-sm-9">
                    <h3 class="portlet-title"> {{$data->phone}}</h5>
                    </div>
                </div>
                <hr>
                @if($data->yearofExp)
                    <div class="row">
                        <div class="col-sm-3">
                        <h3 class="portlet-title">Year of Experience</h5>
                        </div>
                        <div class="col-sm-9">
                        <h3 class="portlet-title"> {{$data->yearofExp}}</h5>
                        </div>
                    </div>
                    <hr>
                @endif
                @if($data->experienceLetter)
                    <div class="row">
                        <div class="col-sm-3">
                        <h3 class="portlet-title">Experience Letter</h5>
                        </div>
                        <div class="col-sm-9">
                        <h3 class="portlet-title">{{$data->experienceLetter}}</h5>
                        </div>
                    </div>
                    <hr>
                @endif
                <div class="row">
                    <div class="col-sm-3">
                    <h3 class="portlet-title">Postal Code</h5>
                    </div>
                    <div class="col-sm-9">
                    <h3 class="portlet-title">{{$data->postalcode}}</h5>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-sm-3">
                    <h3 class="portlet-title">State</h5>
                    </div>
                    <div class="col-sm-9 ">
                    <h3 class="portlet-title">{{$data->state}}</h5>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-sm-3">
                    <h3 class="portlet-title">Street</h5>
                    </div>
                    <div class="col-sm-9">
                    <h3 class="portlet-title">{{$data->street}}</h5>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-sm-3">
                    <h3 class="portlet-title">Apartment</h5>
                    </div>
                    <div class="col-sm-9">
                    <h3 class="portlet-title"> {{$data->apartment}}</h5>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-sm-3">
                    <h3 class="portlet-title">Town</h5>
                    </div>
                    <div class="col-sm-9">
                    <h3 class="portlet-title">{{$data->town}}</h5>
                    </div>
                </div>
                <hr>
                @if($data->yearofExp)
                    <div class="row">
                        <div class="col-sm-3">
                        <h3 class="portlet-title">Photo</h5>
                        </div>
                        <div class="col-sm-9">
                            <img src="{{ Storage::disk('logo')->url($data['image']) }}" alt="image of {{$data->name}}" width="100px" height="100px">
                        </div>
                    </div>
                    <hr>
                @endif
                @if($doc)
                    <div class="row">
                        <div class="col-sm-3">
                        <h3 class="portlet-title">Offer Letter</h5>
                        </div>
                        <div class="col-sm-9">
                            @foreach($doc as $document)
                                <a href="{{ Storage::disk('offer_letters')->url($document['offerLetter']) }}" target="_blank">{{$document->offerLetter}}</a><br>
                            @endforeach                        
                        </div>
                    </div>
                    <hr>
                @endif

            </div>
        <!-- </div> -->
    </div>
</div>
@endsection

@push('scripts')
<script type="text/javascript">
$(document).ready(function() {  
    var hospitalID;
    $('.custom-file input').change(function (e) {
        var files = [];
        for (var i = 0; i < $(this)[0].files.length; i++) {
            files.push($(this)[0].files[i].name);
        }
        $(this).next('.custom-file-label').html(files.join(', '));
    });
    $(".js-example-tokenizer").select2({
        tags: true,
        tokenSeparators: [',', ' '],
    });
    $("#submit").click(function (event) {
        // Prevent actual form submission
        event.preventDefault();
    
        // disabled the submit button
        $("#submit").prop("disabled", true);

        // Get form
        var form = $('#frm-example')[0];

        // FormData object 
        var data = new FormData(form);

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        });

        $.ajax({
         type: "POST",
         enctype: 'multipart/form-data',
         url: "{{ route('admin.selectedHospital') }}",
         data: data,
         processData: false,
         contentType: false,
         cache: false,
         timeout: 800000,
         success: function (data) {
            $("#submit").prop("disabled", false);
            $('#frm-example')[0].reset();
         },
         error: function (e) {
            console.log("ERROR : ", e);
            $("#submit").prop("disabled", false);
         }
      });
    });   
    $("#upload").click(function (event) {
        // Prevent actual form submission
        event.preventDefault();
    
        // disabled the submit button
        $("#upload").prop("disabled", true);

        // Get form
        var form = $('#frm-upload')[0];

        // FormData object 
        var data = new FormData(form);

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        });

        $.ajax({
         type: "POST",
         enctype: 'multipart/form-data',
         url: "{{ route('admin.candidateDocUpload') }}",
         data: data,
         processData: false,
         contentType: false,
         cache: false,
         timeout: 800000,
         success: function (data) {
            $("#upload").prop("disabled", false);
            $('#frm-upload')[0].reset();
         },
         error: function (e) {
            console.log("ERROR : ", e);
            $("#upload").prop("disabled", false);
         }
      });
    });
});
</script>
@endpush
<!-- END Page Content -->

