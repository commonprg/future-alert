@extends('theme.layouts.app')
@section('headerClass','')
@section('content')
 <section class="section">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 text-center">
				<form name='razorpayform' action="payment-verify" method="POST">
				    <input type="hidden" name="razorpay_payment_id" id="razorpay_payment_id">
				    <input type="hidden" name="razorpay_signature"  id="razorpay_signature" >
				</form>
			</div>
		</div>
	</div>
</section>
<script src="https://checkout.razorpay.com/v1/checkout.js"></script>
<script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
<script>
// Checkout details as a json
var options = <?php echo $json ?>;

/**
 * The entire list of Checkout fields is available at
 * https://docs.razorpay.com/docs/checkout-form#checkout-fields
 */
options.handler = function (response){
    document.getElementById('razorpay_payment_id').value = response.razorpay_payment_id;
    document.getElementById('razorpay_signature').value = response.razorpay_signature;
    document.razorpayform.submit();
};

// Boolean whether to show image inside a white frame. (default: true)
options.theme.image_padding = false;

options.modal = {
    ondismiss: function() {
        window.location = '/checkout'
        // console.log("This code runs when the popup is closed");
    },
    // Boolean indicating whether pressing escape key 
    // should close the checkout form. (default: true)
    escape: true,
    // Boolean indicating whether clicking translucent blank
    // space outside checkout form should close the form. (default: false)
    backdropclose: false
};

var rzp = new Razorpay(options);

$(document).ready(function(){
    rzp.open();
});
</script>
@endsection