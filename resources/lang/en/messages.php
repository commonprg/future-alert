<?php

return [
    'welcome' => 'Welcome to our application!',
    'top-para' => 'EDU LOAN IS THE RIGHT OF STUDENTS – IF YOU BELIVE THIS, WE WILL HELP YOU TO PROTECT YOUR
    RIGHT. ASSURE YOU PROPER GUIDANCDE & ASSISATNCE FOR PROSESSING EDU LOAN & C S R
    SCHOLARSHIP APPLICATIONS THROUGH ONLINE.',
    'top-para-sub' => 'Future @ Alert is a private agency intended to provide guidance and help the young aspirants in facilitating the process of their edu loan and C S R scholarship application easily through online.  If meritorious poor students are harassed, refuses edu loan, if bank violates RBI / IBA guidelines, we will provide you up to date guidelines to take up the matter with higher authorities of bank and RBI for redresser of their grievances. We will help and support you in implementing RTI Act 2005 for the purpose of obtaining edu loans. For this purpose we will draft various complaints, applications under RTI Act 2005 etc, if required.  In case if anybody wants to approach judiciary for any grievance with regard to edu loans, we will help you to find out the proper lawyer and assist you. Lawyer fees should be an additional expenditure for you.   Your guts and efforts are more important for obtaining edu loans. Please note that we do not undertake to get any loans and scholarships',
    'top-para-sub-1' => '',
    'ui-para' => 'Benefit of model educational loan scheme through online application:',
    'option-1' => 'The Department of Financial Services has made it mandatory for all banks to accept education loan applications only through online.',
    'option-2' => 'Submit edu loan application through online with three banks at one time.',
    'option-3' => 'Your login id and password help you to monitor the process of your edu loan application.',
    'option-4' => 'No bank managers can reject / refuse any edu loan application without the consent of his / her higher authorities. ',
    'option-5' => 'Your edu loan application will be sanctioned / rejected within 15 – 30 days’ time period. If rejected the reason should be mentioned while rejecting it.',
    'option-6' => 'Edu loans below Rs.7.5 lakhs co – lateral security is not required. ',
    'option-7' => 'Parent’s CBIL score or liability will not be considered.',
    'option-8' => 'Pan Card is mandatory for applying for edu loans.',
    'option-9' => 'In case of injustice by the concerned bank managers, the applicant students or parents can approach the higher authorities of the bank for redresser of the grievances. Nobody will stop you from this.',
    'option-10' => 'Service of Future @ Alert is available in all regional languages.',
    'last-para' => 'Your journey for obtaining edu loans begins when you apply for edu loans through online. We will be with you as a co passenger till you reach the destination. To become professional, choose a course according to your taste and complete your professional education through edu loan facility offered by   Reserve Bank of India.',
    'last-para-01' => ''
];