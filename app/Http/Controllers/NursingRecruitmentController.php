<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Mail\RegisterEmail;
use App\Models\User;
use App\Services\SMSService;
use Session;

class NursingRecruitmentController extends Controller
{
    private $view = 'theme.';

    public function index() {
        return view($this->view.'recruitment');
    }
    public function store(Request $request) {
        $request->validate([
            'firstname' => 'required|regex:/^[a-zA-Z]+$/u',
            'lastname' => 'required|regex:/^[a-zA-Z]+$/u',
            'postcode' => 'required|integer',
            'street' => 'required',
            'city' => 'required',
            'state' => 'required',
            'phone' => 'required|integer|digits_between:10,13',
            'email'    => 'required|unique:users,email,',
            'regNo' => 'required|integer|digits_between:6,15',
            'yearofExp' => 'required|integer|digits_between:1,2',
            'document' => 'required|mimes:pdf|max:2048',
            'image' => 'required|mimes:jpeg,jpg,png|max:2048',
        ],
        [
            'required'  => 'The :attribute field is required.',
            'unique'    => ':attribute is already used'
        ]);

        $data = new User;

        if ($request->file('document') && $request->file('image')) {

            $doc_extn = $request->document->extension() ? $request->document->extension() : ($request->document->getClientOriginalExtension() ? $request->document->getClientOriginalExtension() : null);
            $img_extn = $request->image->extension() ? $request->image->extension() : ($request->image->getClientOriginalExtension() ? $request->image->getClientOriginalExtension() : null);
            
            if($doc_extn && $img_extn) {            
                $experienceLetter = 'document' . time() . '.' . $doc_extn;
                $proFilePhoto = 'image' . time() . '.' . $img_extn;
                $store_original_img = $request->image->storeAs('profile_photos', $proFilePhoto, 'public');
                $store_original = $request->document->storeAs('experience_letters', $experienceLetter, 'public');
            }
            // $Document_name = time()."-".$request->file('document')->getClientOriginalName();
            // $Image_name = time()."-".$request->file('image')->getClientOriginalName();
            
            // $DocumentPath = $request->file('document')->storeAs('experienceLetter', $Document_name, 'public');
            // $ImagePath = $request->file('image')->storeAs('images', $Image_name, 'public');
        }

        $email = User::where('email', $request->email)->first();
        $password = Str::random(10);
    
        // if($email == null){
            $data->firstname = $request->firstname;
            $data->lastname = $request->lastname;
            $data->street = $request->street;
            $data->apartment = $request->address2;
            $data->town = $request->city;
            $data->postalcode = $request->postcode;
            $data->state = $request->state;
            $data->phone = $request->phone;
            $data->email = $request->email;
            $data->regNo = $request->regNo;
            $data->password = \Hash::make($password);
            $data->yearofExp = $request->yearofExp;
            $data->name = $request->firstname.' '.$request->lastname;
            $data->experienceLetter = $experienceLetter;
            $data->usertype = "user";
            $data->category = "nursing recruitment";
            $data->image = $proFilePhoto;
            $data->save();
        // } else {
        //     User::where('email', $request->email)->update(
        //         [
        //             'firstname' => $request->firstname,
        //             'lastname' => $request->lastname,
        //             'street' => $request->street,
        //             'apartment' => $request->address2,
        //             'town' => $request->city,
        //             'postalcode' => $request->postcode,
        //             'state' => $request->state,
        //             'phone' => $request->phone,
        //             'regNo' => $request->regNo,
        //             'yearofExp' => $request->yearofExp,
        //             'name' => $request->firstname.' '.$request->lastname,
        //             'experienceLetter' => $experienceLetter,
        //             'usertype' => "user",
        //             'category' => "edu-loan, nursing recruitment",
        //             'image' => $proFilePhoto,
        //         ]
        //     );
        // }

        $mailData = array('name' => $request->firstname. ' ' .$request->lastname, 'email' => $request->email, 'password' => $password);
              
        \Mail::to($request->email)->send(new RegisterEmail($mailData));
        //Send Text Message to User
        $smsData = array(
            "name" => $request->firstname.' '.$request->lastname,
            "phone" => $request->phone,
            "message" => "Hello ".$request->firstname.' '.$request->lastname.".Thank you for registering.",
        );

        $SMSService = new SMSService();
        $SMSService->sendSMS($smsData);

        Session::flash('success', 'Your data is successfully added to our record. Our representative will contact you soon.');
        // return redirect()->back();
        return redirect()->route('login');
    }
}
