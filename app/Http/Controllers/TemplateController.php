<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mail\RegisterEmail;
use Mail;
use Illuminate\Support\Str;
// use Illuminate\Support\Facades\Mail;
use Stichoza\GoogleTranslate\GoogleTranslate;

class TemplateController extends Controller
{
    private $view = 'theme.';

    public function Home()
    {
        return view($this->view.'index');
    }
    // public function login()
    // {
    //     return view($this->view.'login');
    // }
    public function checkouts()
    {
        return view($this->view.'checkout');
    }
    public function resetpassword()
    {
        return view($this->view.'resetpassword');
    }

    public function contactUs()
    {
        return view($this->view.'contact-us');
    }

    public function privacyPolicy()
    {
        return view($this->view.'privacy-policy');
    }

    public function pricing()
    {
        return view($this->view.'pricing');
    }

    public function aboutUs(Request $request)
    {
        $lan = $request->change_language ? $request->change_language : 'en';
        $tr = new GoogleTranslate($lan); // Translates to 'en' from auto-detected language by default
        return view($this->view.'about')->with(compact('tr'));
    }
    
    public function termsCondition()
    {
        return view($this->view.'teams-condition');
    }

    public function addUser(Request $request)
    {
        $request->validate([
            'firstname' => 'required|regex:/^[a-zA-Z]+$/u',
            'lastname' => 'required|regex:/^[a-zA-Z]+$/u',
            'street' => 'required',
            'city' => 'required',
            'postcode' => 'required|integer',
            'state' => 'required',
            'phone' => 'required|integer|digits_between:10,13',
            'email'    => 'required|unique:users,email'
        ],
        [
            'required'  => 'The :attribute field is required.',
            'unique'    => ':attribute is already used'
        ]);

        $Firstname = $request->input('firstname');
        $Lastname = $request->input('lastname');
        $Address1 = $request->input('street');
        $Address2 = $request->input('address2');
        $City = $request->input('city');
        $Postcode = $request->input('postcode');
        $State = $request->input('state');
        $Phone = $request->input('phone');
        $Email = $request->input('email');
        $Name = $Firstname.' '.$Lastname; 
        $Password = str::random(10);
        $usertype = "user";
        $category = "edu-loan";

        $data = array(
            "name" => $Name,
            "firstname" => $Firstname,
            "lastname" => $Lastname,
            "street" => $Address1,
            "apartment" => $Address2,
            "town" => $City,
            "postalcode" => $Postcode,
            "state" => $State,
            "phone" => $Phone,
            "email" => $Email,
            "usertype" => $user,
            "category" => $category,
            'password' => \Hash::make($Password),
        );

        $Maildata = array('name'=>$Name,'email'=>$Email,'password'=>$Password);
              
        \Mail::to($Email)->send(new RegisterEmail($Maildata));

        \DB::table('users')->insert($data);

        return redirect()->route('login');
    }

    public function pscCoaching(){
        return view($this->view.'psc');
    }
    public function ketCoaching(){
        return view($this->view.'ket');
    }
}
