<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use App\Models\User;
use App\Models\Meeting;
use App\Models\Hospital;
use App\Models\Document;
use App\Jobs\SendEmailJob;
use App\Mail\ReplyEmail;
use App\Mail\RegisterEmail;
use App\Mail\MeetingEmail;
use Mail;
use App\Services\SMSService;

class AdminController extends Controller
{
    private $view = 'admin.';

    public function index()
    {
        return view($this->view.'dashboard');
    }
    public function adminIndex()
    {
        return view($this->view.'index');
    }
    public function getUsers(Request $request)
    {
        if ($request->ajax()) {
            $data = User::latest()->where('usertype', 'user')->get();
            return Datatables::of($data)
            ->addIndexColumn()
            ->make(true);
        }
        return view($this->view.'user');
    }
    public function Meeting(Request $request)
    {
        if ($request->ajax()) {        
            if(!empty($request->from_date))
            {  
                $items = User::where('usertype', '=', 'user')->where('category', '=', 'edu-loan')->whereBetween('created_at', array($request->from_date, $request->to_date))->get();
                return Datatables::of($items)
                ->addColumn('checkbox', function ($item) {
                        return '<input type="checkbox" id="'.$item->id.'" name="checkbox" />';
                })
                ->rawColumns(['checkbox'])
                ->addIndexColumn()
                ->make(true);
            } else { 
                $items = User::where('usertype', '=', 'user')->where('category', '=', 'edu-loan')->get();
                return Datatables::of($items)
                ->addColumn('checkbox', function ($item) {
                        return '<input type="checkbox" id="'.$item->id.'" name="checkbox" />';
                })
                ->rawColumns(['checkbox'])
                ->addIndexColumn()
                ->make(true);
            }
        }     
        return view($this->view.'createmeeting');
    }
    public function crtMeeting(Request $request) {
        if ($request->ajax()) {  
            $request->validate([
                'Meeting_link' => 'required',
                'Meeting_title' => 'required',
                'Meeting_date' => 'required',
            ],
            [
                'required'  => 'The :attribute field is required.',
                'unique'    => ':attribute is already used'
            ]);
         
            $User_id = [];

            $data = array(
                "link" => $request->Meeting_link,
                "title" => $request->Meeting_title,
                "date_time" => $request->Meeting_date,
                "status" => "pending",
            );
            $Meeting = Meeting::insert($data);

            $Meeting_id = \DB::getPDO()->lastInsertId();

            $User_id = $request->id;

            foreach($User_id as $userID){
                $data = array(
                    "meeting_id" => $Meeting_id,
                    "user_id" => $userID,
                );
                $MeetingUser = \DB::table('meeting_user')->insert($data);
                $userDetails = User::find($userID);
                $mailData = array('name' => $userDetails['name'], 'email' => $userDetails['email'], 'link' => $request->Meeting_link, 'title' => $request->Meeting_title, 'date_time' => $request->Meeting_date,);
                Mail::to($userDetails['email'])->send(new MeetingEmail($mailData));   
                //Send Text Message to User
                $smsData = array(
                    "name" => $userDetails['name'],
                    "phone" => $userDetails['phone'],
                    "message" => "Hello ".$userDetails['name'],
                );
        
                $SMSService = new SMSService();
                $SMSService->sendSMS($smsData);
            }
        }     
    }
    public function ParticipantsView($id)
    {
        if ($id) { 
            $items = \DB::table('users')
            ->join( 'meeting_user', function($join) use($id) {
                $join->on('users.id', 'meeting_user.user_id')
                     ->where('meeting_user.meeting_id', $id);
            })
            ->join( 'meetings', 'meeting_user.meeting_id' , '=' , 'meetings.id')
            ->select('users.name','users.email','users.phone','users.category')
            ->get();
            return Datatables::of($items)
            ->addIndexColumn()
            ->make(true);
        }     
    }
    public function getMeetings(Request $request)
    {
        if ($request->ajax()) {
            $data = Meeting::get();
            return Datatables::of($data)
            ->addIndexColumn()
            ->make(true);
        }
        return view($this->view.'meeting');
    }
    public function viewParticipants($id)
    {    
        $data = Meeting::find($id);
        return view($this->view.'meeting_participants')->with(compact('id','data'));
    }
    public function editMeeting($id){
        $data = Meeting::find($id);
        return view($this->view.'edit-meeting')->with(compact('id','data'));
    } 
    public function meetingupdate(Request $request,$id) {  
        $request->validate([
            'title' => 'required',
            'link' => 'required',
            'date_time' => 'required',
            'status' => 'required',
        ],
        [
            'required'  => 'The :attribute field is required.',
            'unique'    => ':attribute is already used'
        ]);
        Meeting::find($id)->update(
            [
                'title'=>$request->title,
                'link' => $request->link,
                'date_time' => $request->date_time,
                'status' => $request->status,
            ]
        );
        return view($this->view.'meeting');
    }
    public function getCandidates(Request $request)
    {
        if ($request->ajax()) {
            $data = User::where('category','nursing recruitment')->get();
            return Datatables::of($data)
            ->addIndexColumn()
            ->make(true);
        }
        return view($this->view.'nursingrecruitment');
    }
    public function getEduloanUsers(Request $request)
    {
        if ($request->ajax()) {
            $data = User::where('category','edu-loan')->get();
            return Datatables::of($data)
            ->addIndexColumn()
            ->make(true);
        }
        return view($this->view.'edu-loan-users');
    }
    public function download($filename){
        try{
            $filepath = storage_path('app/public/experience_letters/').$filename;
            return \Response::download($filepath);
        }catch (\Exception $e){
            report($e);
            echo $e->getMessage();
            return false;
        }
    }
    public function getHospitals(Request $request)
    {
        return view($this->view.'hospitals');
    }
    public function getHospitallist(Request $request) {  
        if ($request->ajax()) {
            $data = Hospital::get();
            return Datatables::of($data)
            ->addIndexColumn()
            ->make(true);
        }
    }
    public function addHospital(Request $request) {
        return view($this->view.'addHospital');
    }
    public function addHos(Request $request) {
        $request->validate([
            'name' => 'required|regex:/^[a-zA-Z0-9]+$/u',
            'Address' => 'required',
            'postcode' => 'required|integer',
            'state' => 'required',
            'phone' => 'required|integer|digits_between:10,13',
            'email'    => 'required,email',
        ],
        [
            'required'  => 'The :attribute field is required.',
            'unique'    => ':attribute is already used'
        ]);

        $data = new Hospital;

        $data->address = $request->Address;
        $data->postalcode = $request->postcode;
        $data->state = $request->state;
        $data->phone = $request->phone;
        $data->email = $request->email;
        $data->name = $request->name;
        $data->save();

        return redirect()->route('admin.getHospitals');
    }
    public function view($id) {    
        $data = Hospital::find($id);
        return view($this->view.'hospital_details')->with(compact('id','data'));
    }
    public function edit($id) {    
        $data = Hospital::find($id);
        return view($this->view.'edit_hospital')->with(compact('id','data'));
    }
    public function update(Request $request,$id) {  
        $request->validate([
            'name' => 'required',
            'Address' => 'required',
            'postcode' => 'required|integer',
            'state' => 'required',
            'phone' => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|max:15',
            'email'    => 'required|regex:/^.+@.+$/i|unique:hospitals,email,'.$id,
        ],
        [
            'required'  => 'The :attribute field is required.',
            'unique'    => ':attribute is already used'
        ]);
        Hospital::find($id)->update(
            [
                'address'=>$request->Address,
                'postalcode' => $request->postcode,
                'state' => $request->state,
                'phone' => $request->phone,
                'email' => $request->email,
                'name' => $request->name,
            ]
        );
        return redirect()->route('admin.getHospitals');
    }
    public function delete($id) {    
        Hospital::find($id)->delete();
        return redirect()->route('admin.getHospitals');
    }
    public function profileView($id) {    
        $data = User::find($id);
        $list = Hospital::get();
        $doc = Document::where('user_id', $id)->get();
        return view($this->view.'candidate_details')->with(compact('id','data','list','doc'));
    }
    public function selectedHospital(Request $request) {
        if ($request->ajax()) {  
            $user_id = $request->user_id;
            $hospital_id = [];
            $hospital_id = $request->hospital_id;
          
            return redirect()->route('login');
        }     
    }
    public function getApplications(Request $request) {
        $data = User::where('usertype', '=', 'user')->get();
        $list = Hospital::get();
        return view($this->view.'getApplications')->with(compact('data','list'));
    }
    public function selectedList(Request $request) {
        if ($request->ajax()) {  
            $user_id = [];
            $user_id = $request->user_id;
            $hospital_id = [];
            $hospital_id = $request->hospital_id;
            foreach($hospital_id as $hospitalID){
                $data = Hospital::find($hospitalID);
                $mailData = [];
                $mailData['email'] = $data['email'];
                $mailData['name'] = $data['name'];
                foreach($user_id as $userID){
                    $user = User::find($userID);
                    $mailData['experience_letter'][]= $user['experienceLetter'];
                }
                SendEmailJob::dispatch($mailData);
            }  
        }     
    }
    public function getShortlistedCandidate(Request $request) {
        $data = User::where('usertype', '=', 'user')->get();
        return view($this->view.'ShortlistedCandidates')->with(compact('data'));
    }
    public function replyEmail(Request $request) {
        if ($request->ajax()) {  
            $user = User::find($request->user_id);
            $mailData = array('name' => $user['name'], 'email' => $user['email'], 'message' => $request['rplyMessage']);
            Mail::to($user['email'])->send(new ReplyEmail($mailData));
            //Send Text Message to User
            $smsData = array(
                "name" => $user['name'],
                "phone" => $user['phone'],
                "message" => "Hello ".$user['name'],
            );
    
            $SMSService = new SMSService();
            $SMSService->sendSMS($smsData);
        }
    }
    public function candidateDocUpload(Request $request){
        $candidate_id = $request->candidate_id;
        $test = $request->validate([
            'customFile' => 'required',
        ],
        [
            'required'  => 'The :attribute field is required.',
        ]);

        if($test) {
            if ($request->file('customFile')) {
                foreach ($request->file('customFile') as $file) {
                    $imageName = 'Offerletter'.'-'.time().'-'.$file->getClientOriginalName();
                    $imageNameArr = [];
                    $imageNameArr[] = $imageName;
                    $data = new Document;
                    $data->user_id = $candidate_id;
                    $data->offerLetter = $imageName;
                    $data->save();
                    $store_original = $file->storeAs('offer_letters', $imageName, 'public');
                }
            }
        }
    }
}
