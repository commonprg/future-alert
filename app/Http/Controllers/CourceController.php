<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use PDF;
use Session;
use App\Models\Document;
use App\Models\User;


class CourceController extends Controller
{
    private $view = 'theme.';

    
    public function checkouts()
    {
        //dd(Auth::user());
        if(Auth::user()){
            return \Redirect::route("dashboard");
        }else{
            return \Redirect::route("checkout");
        }
        return view($this->view.'index');
    }
    public function index()
    {
        if(Auth::user()){
            $userID = Auth::user()->id;
            $data = Document::where('user_id', '=', $userID)->get('offerLetter');
            $category = User::where('id', '=', $userID)->get('category');
            $document = [];
            foreach($data as $doc){
                $document[]= $doc['offerLetter'];
            }
            Session::put('user_data', $document);
                
            return view('dashboard');
        }
    }
    public function docdownload($filename){
        try{
            $filepath = storage_path('app/public/offer_letters/').$filename;
            return \Response::download($filepath);
        }catch (\Exception $e){
            report($e);
            echo $e->getMessage();
            return false;
        }
    }
    public function docview($filename){
        $data = $filename;
        return view('documentview')->with(compact('data'));;
    }
}
