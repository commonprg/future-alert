<?php

namespace App\Http\Controllers;
use URL;
use Mail;
use Session;
use Redirect;
use Razorpay\Api\Api;
use App\Models\Cashfree;
use Illuminate\Support\Str;
use App\Mail\RegisterEmail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Razorpay\Api\Errors\SignatureVerificationError;
use App\Services\SMSService;

class PaymentController extends Controller
{
    private $view = 'theme.';

    public function index(Request $request) {

        $request->validate([
            'firstname' => 'required|regex:/^[a-zA-Z]+$/u',
            'lastname' => 'required|regex:/^[a-zA-Z]+$/u',
            'street' => 'required',
            'city' => 'required',
            'postcode' => 'required|integer',
            'state' => 'required',
            'phone' => 'required|integer|digits_between:10,13',
            'email'    => 'required|unique:users,email'
        ],
        [
            'required'  => 'The :attribute field is required.',
            'unique'    => ':attribute is already used'
        ]);
        
        $request_data = $request->all();

        Session::put('register_data', [
            'firstname'     => $request_data['firstname'],
            'lastname'      => $request_data['lastname'],
            'street'        => $request_data['street'],
            'address2'      => $request_data['address2'],
            'city'          => $request_data['city'],
            'postcode'      => $request_data['postcode'],
            'state'         => $request_data['state'],
            'phone'         => $request_data['phone'],
            'email'         => $request_data['email']
        ]);

        $razorpayApi = new Api(env('RAZORPAY_KEYID'), env('RAZORPAY_KEYSECRET'));

        $orderData = [
            'receipt'           => rand(00000, 99999),
            'amount'            => (int) ($request_data['register_fee']) * 100,
            'currency'          => env('RAZORPAY_CURRENCY'),
            'payment_capture'   => 1
        ];

        $razorpayOrder = $razorpayApi->order->create($orderData);

        $razorpayOrderId = $razorpayOrder['id'];

        Session::put('razorpay_order_id', $razorpayOrderId);

        $displayAmount = $amount = $orderData['amount'];

        $data = [
            "key"               => env('RAZORPAY_KEYID'),
            "amount"            => $amount,
            "name"              => "Future Alert",
            "description"       => "Let's Move Together",
            "image"             => URL::to('/') . "/frontend/assets/images/falert.png",
            "prefill"           => [
                "name"              => $request_data['firstname'] . ' ' . $request_data['lastname'],
                "email"             => $request_data['email'],
                "contact"           => $request_data['phone'],
            ],
            "notes"             => [
                "address"           => $request_data['street'] . ' ' . $request_data['address2'] . ', ' . $request_data['state'] . ', ' . $request_data['city'] . ', ' . $request_data['postcode'],
                "merchant_order_id" => rand(0000000, 9999999),
            ],
            "theme"             => [
                "color"             => "#2f55d4"
            ],
            "order_id"          => $razorpayOrderId,
        ];

        $json = json_encode($data);

        return view('payment.index', ['json' => $json]);
    }

    public function paymentVerify(Request $request) {
        
        $success = true;
        $error = "Payment Failed";
        $request_data = $request->all();
        $register_data = Session::get('register_data');
        if (empty($request_data['razorpay_payment_id']) === false) {

            $razorpayApi = new Api(env('RAZORPAY_KEYID'), env('RAZORPAY_KEYSECRET'));
            try {
                $attributes = array(
                    'razorpay_order_id' => Session::get('razorpay_order_id'),
                    'razorpay_payment_id' => $request_data['razorpay_payment_id'],
                    'razorpay_signature' => $request_data['razorpay_signature']
                );

                $razorpayApi->utility->verifyPaymentSignature($attributes);
            } catch(SignatureVerificationError $e) {
                $success = false;
                $error = 'Razorpay Error : ' . $e->getMessage();
            }
        }

        if ($success === true) {
            $password = Str::random(10);
            $data = [
                "name" => $register_data['firstname'] . ' ' . $register_data['lastname'],
                "firstname" => $register_data['firstname'],
                "lastname" => $register_data['lastname'],
                "street" => $register_data['street'],
                "apartment" => $register_data['address2'],
                "town" => $register_data['city'],
                "postalcode" => $register_data['postcode'],
                "state" => $register_data['state'],
                "phone" => $register_data['phone'],
                "email" => $register_data['email'],
                'password' => \Hash::make($password),
                'usertype' => "user",
                'category' => "edu-loan",
            ];

            $mailData = array('name' => $register_data['firstname'] . ' ' . $register_data['lastname'], 'email' => $register_data['email'], 'password' => $password);
              
            \Mail::to($register_data['email'])->send(new RegisterEmail($mailData));

            //Send Text Message to User
            $smsData = array(
                "name" => $register_data['firstname'].' '.$register_data['lastname'],
                "phone" => $register_data['phone'],
                "message" => "Hello ".$register_data['firstname'].' '.$register_data['lastname'].".Thank you for registering.",
            );

            $SMSService = new SMSService();
            $SMSService->sendSMS($smsData);

            \DB::table('users')->insert($data);

            return redirect()->route('login');
            // $html = "<p>Your payment was successful</p>
            //     <p>Payment ID: {$request_data['razorpay_payment_id']}</p>";
        } else {
            return redirect()->route('payment');
        }

        echo $html;
    }

    // Cashfree Payment gateway start.
    public function cashfree(Request $request) {

        $request_data = $request->all();

        $order_response = Cashfree::createOrder([
            'customer_name'     => $request_data['firstname'] . $request_data['lastname'],
            'customer_phone'    => $request_data['phone'],
            'customer_email'    => $request_data['email']
        ]);

        if($order_response->status == 'success') {
            return Redirect::to($order_response->payment_link);
        } else {
            echo "<pre>";print_r($order_response->msg);exit;
        }
    }

    public function paymentResponse(Request $request) {

        $payment_response = $request->all();
        
        if($payment_response['txStatus'] == 'SUCCESS') {
            echo "<pre>";print_r('Payment Success with Reference Id: ' . $payment_response['referenceId']);exit;
        } else {
            echo "<pre>";print_r($payment_response['txMsg']);exit;
        }
    }

    public function refundOrder() {
        
        $refund_response = Cashfree::refund([
            'reference_id'      => '1107547',
            'refund_amount'     => '100',
            'refund_note'       => 'Refund Note'
        ]);

        if($refund_response->status == 'success') {
            echo "<pre>";print_r($refund_response->msg . ' with Refund Id: ' . $refund_response->refund_id);exit;
        } else {
            echo "<pre>";print_r('ERROR: ' . $refund_response->msg);exit;
        }
    }
    // Cashfree payment gateway end.

}
