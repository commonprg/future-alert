<?php

namespace App\Models;

use URL;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Http;

class Cashfree extends Model
{
    use HasFactory;

    public static function createOrder($params = []) {
        $response = Http::asForm()->post(env('CASHFREE_ENDPOINT') . 'api/v1/order/create', [
            'appId'         => env('CASHFREE_APPID'),
            'secretKey'     => env('CASHFREE_SECRET'),
            "orderId"       => rand(000001, 999999),
            "orderAmount"   => env('REGISTER_FEE'),
            'customerName'  => $params['customer_name'],
            "customerPhone" => $params['customer_phone'],
            'customerEmail' => $params['customer_email'],
            "returnUrl"     => URL::to('/') . '/payment-response'
        ]);

        $response_data = json_decode($response->body());

        if($response_data->status == 'OK') {
            return (object) [
                'status'        => 'success',
                'payment_link'  => $response_data->paymentLink
            ];
        } else {
            return (object) [
                'status'    => 'error',
                'msg'       => $response_data->reason
            ];
        }
    }

    public static function refund($params = []) {
        $response = Http::asForm()->post(env('CASHFREE_ENDPOINT') . 'api/v1/order/refund', [
            'appId' => env('CASHFREE_APPID'),
            'secretKey' => env('CASHFREE_SECRET'),
            'referenceId' => $params['reference_id'],
            'refundAmount' => $params['refund_amount'],
            'refundNote' => $params['refund_note']
        ]);

        $response_data = json_decode($response->body());

        if($response_data->status == 'OK') {
            return (object) [
                'status'        => 'success',
                'msg'           => $response_data->message,
                'refund_id'     => $response_data->refundId
            ];
        } else {
            return (object) [
                'status'    => 'error',
                'msg'       => $response_data->message
            ];
        }
    }
}
