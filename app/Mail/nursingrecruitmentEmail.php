<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class nursingrecruitmentEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $data;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $mail = $this->from('noreply@falert.in','Future-alert')
        ->subject('BSc.Nursing Candidate')
        ->view('admin.nursingrecruitment-email')
        ->with([
            'email' => $this->data['email'],
            'name' => $this->data['name'],
        ]);
        
        if(!empty($this->data['experience_letter'])){
            foreach($this->data['experience_letter'] as $k => $v){
                $path = storage_path('app/public/experience_letters/').$v;
                $filename = basename($v, ".pdf");
                $mail = $mail->attach($path, [
                    'as' => $filename,
                    'mime' => 'file-mime-type',
                ]);
            }
        }

        return $mail;
    }
}
