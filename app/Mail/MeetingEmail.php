<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class MeetingEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $data;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->email = $data['email'];
        $this->name = $data['name'];
        $this->link = $data['link'];
        $this->title = $data['title'];
        $this->date_time = $data['date_time'];
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('noreply@falert.in','Future-alert')
        ->subject('Nursing Recruitment Selection Confirmation')
        ->view('admin.meeting-email')
        ->with([
            'email' => $this->email,
            'name' => $this->name,
            'link' => $this->link,
            'title' => $this->title,
            'date_time' => $this->date_time,
        ]);
    }
}
