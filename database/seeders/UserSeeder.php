<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

use App\Models\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User;
        $user->name = 'Admin';
        $user->firstname = 'Monis';
        $user->lastname = 'Thomas';
        $user->password = Hash::make('allen123$');
        $user->email = 'thomasthomas184@gmail.com';
        $user->usertype = 'admin';
        $user->save();
    }
}
